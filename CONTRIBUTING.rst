CONTRIBUTING
============

To contribute code or documentation, submit a pull request at the
`source repository website
<https://code.uplex.de/uplex-varnish/libvmod-ece>`_.

If you have a problem or discover a bug, you can post an `issue
<https://code.uplex.de/uplex-varnish/libvmod-ece/issues>`_ at
the website. You can also write to <varnish-support@uplex.de>.

For developers
--------------

The VMOD source code is in C, and compilation has been tested with gcc
and clang. The code MUST always compile successfully with both of
them.

The build specifies C99 conformance for C sources (``-std=c99``). All
compiler warnings are turned on, and all warnings are considered
errors (``-Werror -Wall -Wextra``).  The code MUST always build
without warnings or errors under these constraints.

By default, ``CFLAGS`` is set to ``-g -O2``, so that symbols are
included in the shared library, and optimization is at level
``O2``. To change or disable these options, set ``CFLAGS`` explicitly
before calling ``configure`` (it may be set to the empty string).

For development/debugging cycles, the ``configure`` option
``--enable-debugging`` is recommended (off by default). This will turn
off optimizations and function inlining, so that a debugger will step
through the code as expected.

Experience has shown that adding ``-ggdb3`` to ``CFLAGS`` is
beneficial if you need to examine the VMOD with the gdb debugger. The
shared object for a VMOD is loaded from a directory relative to the
Varnish home directory (by default
``/usr/local/var/varnish/$INSTANCE`` for development builds). A
debugger needs to locate the shared object from that relative path to
load its symbols, so the Varnish home directory should be the current
working directory when the debugger is run. For example::

  # To run gdb and examine a coredump
  $ cd /usr/local/var/myinstance
  $ gdb /usr/local/sbin/varnishd /path/to/coredump

By default, the VMOD is built with the stack protector enabled
(compile option ``-fstack-protector``), but it can be disabled with
the ``configure`` option ``--disable-stack-protector``.

Setting the salt for testing
============================

The salt bytes specified by RFC 8188 are usually generated for an
encrypted message from the ``libcrypto`` pseudo-random number
generator. The test scripts include tests against known answers given
in RFC 8188, for which a fixed salt is specified. To test message
encryption against the known answer, it is necessary to set the salt
bytes.

This is possible when the ``configure`` script is invoked with
``--enable-set-salt``. When set-salt is enabled, a base64-encoded salt
value may be set in the backend request header with the fixed name
``XYZZY-ECE-Salt``::

  sub vcl_backend_response {
	# Set the salt bytes specified in the RFC 8188 examples.
        set bereq.http.XYZZY-ECE-Salt = "I1BsxtFttlv3u/Oo94xnmw==";
        set beresp.filters = "ece_encrypt";
  }

The base64 decoding function from ``libcrypto`` is used to decode the
header contents. This function uses the alphabet ``[a-ZA-Z0-9/+]`` and
``=`` padding, just like the ``BASE64`` encoding scheme used by VMOD
blob in the standard Varnish distribution. Since the salt is specified
by RFC 8188 to be 16 bytes long, the base64 encoding is always 24
characters long, including two ``=`` padding characters.

For encryption, when the set-salt feature is enabled, the VFP looks
for the header, and if it is present, the salt is set from the
header's base64-decoded contents. If the header is not present, then
random salt is generated as in normal operation.

The tests in ``src/tests`` that require specific salt are skipped
unless the environment variable ``ENABLE_SET_SALT`` is set to
``yes``. When ``configure`` is invoked with ``--enable-set-salt``,
``automake`` ensures that ``ENABLE_SET_SALT=yes`` when ``make check``
is invoked.

The set-salt feature should *not* be enabled for production builds.
It should be used *only* for testing purposes.

Test coverage
=============

.. _lcov: http://ltp.sourceforge.net/coverage/lcov.php

The autotools generate a make target ``coverage``, which can be used
to generate test coverage reports, if you have the tools ``lcov`` and
``genhtml`` installed (see `lcov`_). HTML coverage reports are
generated in ``src/coverage``::

  $ make coverage
  # Now point your browser to src/coverage/index.html

By default, ``configure`` checks if both of ``lcov`` and ``genhtml``
can be found on the ``PATH``; if they cannot be found, then coverage
reports cannot be generated (and ``make coverage`` results in an
error). If necessary, you can invoke ``configure`` with
``--with-lcov=/path/to/lcov`` and/or
``--with-genhtml=/path/to/genhtml`` to specify their locations.

Invoking ``make coverage`` has these effects:

* ``make clean`` is invoked.

* Code is compiled with ``CFLAGS`` set to generate ``gcov`` output.
  This entails disabling debugging and optimization.

* ``make check`` is invoked.

* ``lcov`` and ``genhtml`` are used to generate the reports.

After running ``make coverage``, it may be necessary to clean up with
``make clean`` and rebuild the code before you continue development
and debugging, since the symbols for ``gcov`` may interfere with
linkage and testing.
