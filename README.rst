..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit vmod.vcc and run make instead
..

.. role:: ref(emphasis)

========
vmod_ece
========

----------------------------------------------------------
Varnish Module for encrypted Content-Encoding per RFC 8188
----------------------------------------------------------

:Manual section: 3


SYNOPSIS
========

::

  import ece;

  # The built-in "ece_decrypt" filter decrypts backend responses.
  sub vcl_backend_response {
	if (beresp.http.Content-Encoding == "aes128gcm") {
		set beresp.filters = "ece_decrypt";
	}
  }

  # VMOD version
  STRING ece.version()

DESCRIPTION
===========

VMOD ece is a Varnish module that supports the use of encrypted
content encoding. It enables configurations that implement and
interpret Content-Encoding ``aes128gcm``, by providing these
resources:

* a Varnish Fetch Processor (VFP) to decrypt backend responses

* XXX

Encrypted Content-Encoding for HTTP is specified in `RFC
8188`_. Details of the encoding are beyond the scope of this manual;
users of this library are advised to consult the RFC as well, to fully
understand how to use it properly and securely.

XXX ...

Encryption and HTTP
--------------------

XXX ...

.. _ece.encrypter():

new xencrypter = ece.encrypter(STRING name, BYTES rs, STRING key_hdr)
---------------------------------------------------------------------

::

   new xencrypter = ece.encrypter(
      STRING name,
      BYTES rs=4096,
      STRING key_hdr="X-ECE-Key-ID"
   )

Create an encryption filter named ``name`` with custom parameters.

XXX ...

.. _xencrypter.create_stats():

VOID xencrypter.create_stats()
------------------------------

Create statistics, observable with a tool like ``varnishstat(1)``, for
the custom encryption filter. These are the same as the counters
created for the standard ``"ece_encrypt"`` filter. See `STATISTICS`_
below for details.

XXX ...

.. _ece.decrypter():

new xdecrypter = ece.decrypter(STRING name, BYTES chunksz, BYTES max_rs)
------------------------------------------------------------------------

::

   new xdecrypter = ece.decrypter(
      STRING name,
      BYTES chunksz=16384,
      BYTES max_rs=1048576
   )

Create a decryption filter named ``name`` with custom parameters.

XXX ...

.. _xdecrypter.create_stats():

VOID xdecrypter.create_stats()
------------------------------

Create statistics for the custom decryption filter, the same as the
counters created for the standard ``"ece_decrypt"`` filter. See
`STATISTICS`_ for details.

XXX ...

.. _ece.add_key():

VOID add_key(STRING id, BLOB key)
---------------------------------

Add the keying material identified by ``id`` with the contents of the
blob ``key``, provided that ``id`` does not already exist.

XXX ...

.. _ece.update_key():

VOID update_key(STRING id, BLOB key)
------------------------------------

Change the keying material identified by ``id`` to the contents of the
blob ``key``, provided that ``id`` already exists.

XXX ...

.. _ece.set_key():

VOID set_key(STRING id, BLOB key)
---------------------------------

Set the keying material identified by ``id`` to the contents of the
blob ``key``. This is the "add-or-update" operation; key ``id`` is
added if it does not already exist, and modified if it already exists.

XXX ...

.. _ece.delete_key():

VOID delete_key(STRING id)
--------------------------

Remove the keying material identified by ``id``.

XXX ...

.. _ece.key_exists():

BOOL key_exists(STRING id)
--------------------------

Returns true iff the keying material identified by ``id`` has been added.

XXX ...

.. _ece.key_added():

TIME key_added(STRING id)
-------------------------

Returns the time at which the keying material identified by ``id`` was
added.

XXX ...

.. _ece.key_updated():

TIME key_updated(STRING id)
---------------------------

Returns the time at which the keying material identified by ``id`` was
last updated.

XXX ...

.. _ece.dump_keys():

VOID dump_keys(ENUM {LOCAL, UTC} tz=LOCAL)
------------------------------------------

Generate a synthetic client response body with information in CSV
format (comma-separated values) about all of the keys that are
currently stored.

XXX ...

.. _ece.libcrypto_version():

STRING libcrypto_version()
--------------------------

Return the libcrypto version string.

Example::

  std.log("Using libcrypto version: " + ece.libcrypto_version());

.. _ece.version():

STRING version()
----------------

Return the version string for this VMOD.

Example::

  std.log("Using VMOD ece version: " + ece.version());

STATISTICS
==========

XXX ...

SECURITY
========

XXX ...

ERRORS
======

XXX ...

REQUIREMENTS
============

The VMOD currently requires the Varnish master branch. It also
requires the ``libcrypto`` library for cryptographic operations, and
has been tested with OpenSSL versions 1.1.1c and 1.1.1d.

XXX ...

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

LIMITATIONS
===========

XXX ...

SEE ALSO
========

* varnishd(1)
* vcl(7)
* `RFC 8188`_

COPYRIGHT
=========

::

  Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 
  See LICENSE
 

.. _RFC 8188: https://tools.ietf.org/html/rfc8188
