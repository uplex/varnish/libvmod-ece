/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "cache/cache_filter.h"

#include "VSC_ece.h"

#define MIN_RS 18

struct vfp_settings {
	unsigned		magic;
#define VFP_SETTINGS_MAGIC	0x00b45435
	const char		*key_hdr;
	size_t			chunksz;
	uint32_t		rs;
};

struct vfp_cfg {
	unsigned		magic;
#define VFP_CFG_MAGIC		0x75413842
	struct vfp_settings	*settings;
	struct VSC_ece		*stats;
	struct vsc_seg		*vsc_seg;
};

/* VFP .fini method for both encrypt and decrypt */
void v_matchproto_(vfp_fini_f) vfp_common_fini(struct vfp_ctx *ctx,
					       struct vfp_entry *ent);

enum vfp_status v_matchproto_(vfp_init_f)
	vfp_encrypt_init(struct vfp_ctx *ctx, struct vfp_entry *ent);

enum vfp_status v_matchproto_(vfp_pull_f)
	vfp_encrypt_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
			 ssize_t *lenp);

enum vfp_status v_matchproto_(vfp_init_f)
	vfp_decrypt_init(struct vfp_ctx *ctx, struct vfp_entry *ent);

enum vfp_status v_matchproto_(vfp_pull_f)
	vfp_decrypt_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
			 ssize_t *lenp);

extern const struct vfp vfp_decrypt, vfp_encrypt;
