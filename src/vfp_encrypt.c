/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include "vfp_common.h"

#define ERR(ctx, msg)				\
	ERR_VFP((ctx), "encrypt", msg)

#define VERR(ctx, fmt, ...)				\
	VERR_VFP((ctx), "encrypt", fmt, __VA_ARGS__)

#define DEFAULT_KEY_HDR "\015X-ECE-Key-ID:"
#define DEFAULT_RS 4096

/*
 * Declare the set_salt() function if set-salt was enabled at configure
 * time.
 */
#ifdef ENABLE_SET_SALT
enum vfp_status set_salt(struct vfp_ctx *ctx, uint8_t *salt);
#endif

/**
 ** VFP encrypt
 **/

/*
 * Return VFP_ERROR on error.
 * VFP_NULL if there is insufficient space in the streams for the next
 * encrypted record.
 * Otherwise the vp status passed in (from suck in the pull method).
 */
static enum vfp_status
encrypt(struct vfp_ctx *ctx, struct ece *ece, enum vfp_status vp)
{
	struct ece_stream *stream;
	struct ece_crypto *crypto;
	size_t record_len, plaintext_len;
	ssize_t ciphertext_len;
	unsigned char nonce[NONCE_LEN];
	char errmsg[ERRMSG_LEN];
	int last = (vp == VFP_END);

	CHECK_OBJ_NOTNULL(ece, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->crypto, ECE_CRYPTO_MAGIC);
	CHECK_OBJ_NOTNULL(ece->stream, ECE_STREAM_MAGIC);
	stream = ece->stream;
	crypto = ece->crypto;

	record_len = ece->rs;
	if (stream->avail_in < ece->chunksz) {
		if (vp != VFP_END)
			return (VFP_NULL);
		AN(last);
		record_len = stream->avail_in + TAG_LEN + 1;
	}
	assert(record_len <= ece->rs);
	if (record_len > stream->avail_out) {
		assert(vp != VFP_END);
		return (VFP_NULL);
	}
	plaintext_len = record_len - (TAG_LEN + 1);
	if (plaintext_len > stream->avail_in) {
		assert(vp != VFP_END);
		return (VFP_NULL);
	}

	nonce_xor_seq(crypto, nonce);
	ciphertext_len = encrypt_record(ece->ectx, stream->next_in,
					plaintext_len,
					plaintext_len + TAG_LEN + 1,
					crypto->cek, nonce, last,
					stream->next_out, errmsg);
	seq_inc(crypto);

	if (ciphertext_len < 0)
		return (VERR(ctx, "%s", errmsg));

	stream->avail_in  -= plaintext_len;
	stream->next_in   += plaintext_len;
	stream->avail_out -= ciphertext_len;
	stream->next_out  += ciphertext_len;

	return (vp);
}

enum vfp_status v_matchproto_(vfp_init_f)
vfp_encrypt_init(struct vfp_ctx *ctx, struct vfp_entry *ent)
{
	struct ece *ece = NULL;
	struct vfp_settings *settings;
	struct ece_hdrbuf *hdrbuf;
	const struct vfp_cfg *cfg;
	const char *keyid;
	size_t keyid_len;
	char errmsg[ERRMSG_LEN];

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);
	CHECK_OBJ_NOTNULL(cfg->settings, VFP_SETTINGS_MAGIC);
	settings = cfg->settings;

	/* XXX implement me */
	if (http_GetStatus(ctx->resp) == 206)
		return (VFP_NULL);

	if (http_GetHdr(ctx->resp, H_Content_Encoding, NULL))
		return (VFP_NULL);

	if (cfg->stats != NULL)
		cfg->stats->ops++;

	if (http_GetHdr(ctx->req, settings->key_hdr, &keyid) == 0)
		return (VERR(ctx, "key id header %s not found",
			     settings->key_hdr + 1));
	keyid_len = strlen(keyid);
	if (keyid_len > MAX_ID_LEN)
		return (VERR(ctx, "key id \"%.80s...\" too long "
			     "(length %zd > 255)", keyid, keyid_len));

	if (common_alloc(ctx, &ece, 1) == VFP_ERROR)
		return (VFP_ERROR);
	CHECK_OBJ_NOTNULL(ece, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->stream, ECE_STREAM_MAGIC);
	CHECK_OBJ_NOTNULL(ece->crypto, ECE_CRYPTO_MAGIC);
	CHECK_OBJ_NOTNULL(ece->hdr, ECE_HDRBUF_MAGIC);
	ent->priv1 = ece;
	hdrbuf = ece->hdr;

#ifdef ENABLE_SET_SALT
	enum vfp_status salt_status;
	salt_status = set_salt(ctx, hdrbuf->hdr);
	if (salt_status == VFP_ERROR)
		return (salt_status);
	else if (salt_status == VFP_NULL) {
		if (add_salt(hdrbuf->hdr, errmsg) != 0)
			return (VERR(ctx, "%s", errmsg));
	}
#else
	if (add_salt(hdrbuf->hdr, errmsg) != 0)
		return (VERR(ctx, "%s", errmsg));
#endif

	ece->rs = settings->rs;
	encode_header(hdrbuf->hdr, ece->rs, (uint8_t)keyid_len,
		      (uint8_t *)keyid);

	if (crypto_init(ctx, ece->crypto, hdrbuf->hdr, (uint8_t *)keyid,
			(uint8_t)keyid_len, 1) == VFP_ERROR)
		return (VFP_ERROR);
	hdrbuf->next_in = hdrbuf->hdr;
	hdrbuf->avail_in = HDR_PFX_LEN + keyid_len;

	/*
	 * Allocate space for one input record, but only fetch chunksz,
	 * such that that there is enough space for the delimiter byte and
	 * tag. So the input buffer can be safely overwritten with padding
	 * bytes, delimiter and tag.
	 *
	 * XXX allow a configurable max plaintext length per record, which
	 * must be <= rs - (TAG_LEN + 1) and > 1. The extra bytes are
	 * padded in each record (so as to obscure the actual size of the
	 * plaintext message).
	 */
	errno = 0;
	ece->buf = malloc(ece->rs);
	if (ece->buf == NULL)
		return (VERR(ctx, "buffer allocation failed: %s",
			     strerror(errno)));
	ece->chunksz = ece->rs - (TAG_LEN + 1);

	http_SetHeader(ctx->resp, "Content-Encoding: aes128gcm");
	RFC2616_Vary_AE(ctx->resp);
	common_hdr_set(ctx);

	return (VFP_OK);
}

enum vfp_status v_matchproto_(vfp_pull_f)
vfp_encrypt_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
		 ssize_t *lenp)
{
	struct ece *ece;
	struct ece_stream *stream;
	struct ece_hdrbuf *hdrbuf;
	const struct vfp_cfg *cfg;
	struct VSC_ece *stats;
	unsigned char *p = ptr, *inb4;
	enum vfp_status vp = VFP_OK;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	CAST_OBJ_NOTNULL(ece, ent->priv1, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->stream, ECE_STREAM_MAGIC);
	CHECK_OBJ_NOTNULL(ece->hdr, ECE_HDRBUF_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);
	AN(ptr);
	AN(lenp);
	assert(ece->rs > TAG_LEN + 1);

	stream = ece->stream;
	hdrbuf = ece->hdr;
	stats = cfg->stats;

	setOutputBuf(stream, ptr, *lenp);
	if (!isHdrBufEmpty(hdrbuf)) {
		flushHdrBuf(stream, hdrbuf);
		if (isOutputBufFull(stream)) {
			*lenp = outputLen(stream, p);
			return (VFP_OK);
		}
	}

	while (vp == VFP_OK) {
		if (isOutputBufFull(stream))
			break;
		if (isInputBufEmpty(stream)) {
			size_t len = ece->chunksz;

			memset(ece->buf, 0, len);
			vp = suck_bytes(ctx, ece->buf, &len);
			assert(len <= ece->chunksz);
			if (stats != NULL)
				stats->in += len;
			if (vp == VFP_ERROR)
				return (vp);
			setInputBuf(stream, ece->buf, len);
		}
		else {
			inb4 = streamIn(stream);
			vp = fillInputBuf(ctx, stream, ece->chunksz, vp);
			if (stats != NULL)
				stats->in += streamIn(stream) - inb4;
			if (vp == VFP_ERROR)
				return (vp);
		}
		if (!isInputBufEmpty(stream)) {
			enum vfp_status encrypt_status = encrypt(ctx, ece, vp);
			if (encrypt_status == VFP_ERROR)
				return (VFP_ERROR);
			if (encrypt_status == VFP_NULL) {
				/* Output record was buffered */
				*lenp = outputLen(stream, p);
				return (VFP_OK);
			}
		}
		else if (vp != VFP_END)
			ERR(ctx, "message truncated");
	}

	*lenp = outputLen(stream, p);
	return (vp);
}

static struct vfp_settings default_settings = {
	.magic   = VFP_SETTINGS_MAGIC,
	.key_hdr = DEFAULT_KEY_HDR,
	.rs = DEFAULT_RS,
};

static struct vfp_cfg default_cfg = {
	.magic = VFP_CFG_MAGIC,
	.settings = &default_settings,
	.stats = NULL,
	.vsc_seg = NULL,
};

const struct vfp vfp_encrypt = {
	.name = "ece_encrypt",
	.init = vfp_encrypt_init,
	.pull = vfp_encrypt_pull,
	.fini = vfp_common_fini,
	.priv1 = &default_cfg,
};
