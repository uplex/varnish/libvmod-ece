/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Chapter references are to RFC 8188 */

#include <stdint.h>
#include <sys/types.h>

#include <openssl/evp.h>

#include "foreign/vend.h"

#define AES128_KEYLEN 16
#define SALT_LEN 16
#define SHA256_LEN 32
#define NONCE_LEN 12
#define TAG_LEN AES128_KEYLEN

/* ERR_error_string(3): "buf must be at least 256 bytes long." */
#define ERRMSG_LEN 256

/*
 * Functions returning int return 0 on success, non-zero on failure.
 * Every function writes an error message to the errmsg buffer on failure.
 *
 * Buffer parameters in many of the function declarations are expressed
 * here as arrays with sizes, for documentation purposes. Those buffers
 * MUST be allocated with the given size.
 */

/*
 * ch 2.1
 * Input is hdr, outputs are rs and idlen. The salt is the first SALT_LEN
 * bytes of hdr. The keyid is *idlen bytes long, starting at hdr[ID_OFF].
 */
#define RS_OFF 16
#define IDLEN_OFF 20
#define ID_OFF (IDLEN_OFF + 1)

static inline void
decode_header(uint8_t *hdr, uint32_t *rs, uint8_t *idlen)
{
	*rs = vbe32dec(hdr + RS_OFF);
	*idlen = hdr[IDLEN_OFF];
}

/*
 * Output is hdr, inputs are rs, idlen and keyid. Salt is added
 * separately.
 */
void encode_header(uint8_t *hdr, uint32_t rs, uint8_t idlen, uint8_t *keyid);

/* Set SALT_LEN random bytes from the libcrypto CSPRNG at hdr. */
int add_salt(unsigned char *hdr, char errmsg[ERRMSG_LEN]);

/*
 * ch. 2.2 pseudorandom key
 * Inputs are salt and key, output is prk. prk is re-used for both
 * derive_cek() and derive_prenonce() below.
 */
int derive_prk(uint8_t salt[SALT_LEN], uint8_t key[AES128_KEYLEN],
    unsigned char prk[SHA256_LEN], char errmsg[ERRMSG_LEN]);

/*
 * ch. 2.2 content encryption key
 * Input is prk, output is cek. The first AES128_KEYLEN bytes of cek
 * are used as the key.
 */
int derive_cek(unsigned char prk[SHA256_LEN], unsigned char cek[SHA256_LEN],
    char errmsg[ERRMSG_LEN]);

/*
 * ch. 2.3, except this derives a "prenonce", which is constant throughout
 * the en-/decryption of a message. The prenonce is XOR'd with the
 * sequence number to derive the nonce for each record.
 *
 * Input is prk, output is prenonce. The first NONCE_LEN bytes of the
 * per-record nonce is used to en-/crypt each record.
 */
int derive_prenonce(unsigned char prk[SHA256_LEN],
    unsigned char prenonce[SHA256_LEN], char errmsg[ERRMSG_LEN]);

/*
 * Returns an EVP_CIPHER_CTX that may be re-used to encrypt or decrypt the
 * entire sequence of records in a message. enc is 1 for encryption, 0 for
 * decryption.
 *
 * Returns NULL on failure.
 */
EVP_CIPHER_CTX * cipher_ctx_init(int enc, char errmsg[ERRMSG_LEN]);

/* Finalize an EVP_CIPHER_CTX. */
static inline void
cipher_ctx_fini(EVP_CIPHER_CTX *ctx)
{
	EVP_CIPHER_CTX_free(ctx);
}

/*
 * Decrypt a record. Inputs are:
 *	ciphertext, ciphertext_len, tag, cek, nonce
 * Outputs are:
 *	plaintext, last
 *
 * nonce results from XOR-ing the prenonce with the record sequence
 * number.
 *
 * At least rs - TAG_LEN bytes must be allocated for the buffer at plaintext.
 *
 * On return, *last is non-zero if this was the last record of the
 * message, 0 otherwise.
 *
 * Returns -1 on error, otherwise the number of plaintext bytes.
 */
ssize_t decrypt_record(EVP_CIPHER_CTX *ctx, unsigned char *ciphertext,
    int ciphertext_len, unsigned char tag[TAG_LEN], uint8_t cek[AES128_KEYLEN],
    unsigned char nonce[NONCE_LEN], unsigned char *plaintext, int *last,
    char errmsg[ERRMSG_LEN]);

/*
 * Encrypt a record. Inputs are:
 *	plaintext, plaintext_len, rs, cek, nonce, last
 * Outputs is:
 *	ciphertext
 *
 * The buffer at plaintext MUST have at least rs bytes allocated.
 * plaintext is modified for padding during the call; make a copy if the
 * original plaintext must be retained. This is necessary in particular
 * when more than one record is required for a message, since the
 * plaintext for subsequent records must begin at a location that will
 * have been overwritten.
 *
 * plaintext_len MUST be >= 0, and MAY NOT be > rs - (TAG_LEN + 1).
 *
 * rs MUST be >= 18. For every record, rs MUST equal the record size
 * established for the entire message, except possibly for the last
 * record, which may be smaller.
 *
 * last is non-zero iff this is the last record in the message.
 *
 * At least rs bytes must be allocated for the buffer at ciphertext.
 * The authentication tag is appended at ciphertext + (rs - TAG_LEN).
 *
 * Returns -1 on error, otherwise the number of bytes written to
 * ciphertext, including the authentication tag. Successful return values
 * are always equal to rs, and the next record can be written to
 * ciphertext + rs.
 */
ssize_t encrypt_record(EVP_CIPHER_CTX *ctx, unsigned char *plaintext,
    int plaintext_len, uint32_t rs, uint8_t cek[AES128_KEYLEN],
    unsigned char nonce[NONCE_LEN], int last, unsigned char *ciphertext,
    char errmsg[ERRMSG_LEN]);
