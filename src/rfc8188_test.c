/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>

#ifndef VDEF_H_INCLUDED
# include "vdef.h"
#endif
#include "vas.h"

#include "rfc8188.h"

#define HDR_PREFIX_LEN (SALT_LEN + 4 + 1)

/*
 * Input data and expected values from the two examples in RFC8188 ch 3.1
 * and 3.2
 *
 * base64 encodings using libcrypto EVP_En/Decode differ from the examples
 * in RFC8188.
 *
 * The RFC uses base64url, libcrypto does not, so:
 *	'-' => '+'
 *	'_' => '/'
 *
 * libcrypto requires '=' padding, the RFC doesn't use them.
 *
 * The output buffers for libcrypto decodings are larger than the actual
 * binary lengths, because libcrypto pads '\0's so that the output size is
 * exactly input_size*(3/4).
 */

static const unsigned char exp_plaintext[] = "I am the walrus";
static const int exp_plaintext_len = 15;

/* 1st example in ch 3.1 */
static const unsigned char body1_b64[] =
    "I1BsxtFttlv3u/Oo94xnmwAAEAAA+NAVub2qFgBEuQKRapoZu+IxkIva3MEB1PD+ly8Thjg=",
	key1_b64[] = "yqdlZ+tYemfogSmv7Ws5PQ==",
	salt_b64[] = "I1BsxtFttlv3u/Oo94xnmw==",
	prk1_b64[] = "zyeH5phsIsgUyd4oiSEIy35x+gIi4aM7y0hCF8mwn9g=",
	cek1_b64[] = "/wniytB+ofscZDh4tbSjHw==",
	nonce1_b64[] = "Bcs8gkIRKLI8GeI8";
static const int bodylen1 = 53;
static const uint32_t exp_rs1 = 4096;
static const uint8_t exp_idlen1 = 0;

/* 2nd example in ch 3.2 */
const unsigned char body2_b64[] =
    "uNCkWiNYzKTnBN9ji3+qWAAAABkCYTHOG8chz/gnvgOqdGYovxyjuqRyJFjEDyoF1Fvkj6hQPdPHI51OEUKEpgz3SsLWIqS/uA==",
	key2_b64[] = "BO3ZVPxUlnLORbVGMpbT1Q==",
	exp_keyid2[] = "a1";
static const int exp_bodylen2 = 73;
static const uint32_t exp_rs2 = 25;
static const uint8_t exp_idlen2 = 2;

int
main(int argc, char *argv[])
{
	EVP_CIPHER_CTX *ctx;
	unsigned char prk_b64[45], cek_b64[25], nonce_b64[17],
	    body1_test_b64[73], body2_test_b64[101];
	unsigned char key1[AES128_KEYLEN + 2], key2[AES128_KEYLEN + 2],
	    salt[SALT_LEN + 2], prk[SHA256_LEN], cek[SHA256_LEN],
	    nonce[SHA256_LEN], seq[NONCE_LEN],
	    body1[54], body2[75], plaintext[64], *ciphertext;
	char errmsg[ERRMSG_LEN];
	uint32_t rs;
	uint8_t idlen;
	int len, last, plaintext_len, ciphertext_len;

	(void)argc;
	(void)argv;

	/* example 1, ch 3.1 */

	len = EVP_DecodeBlock(key1, key1_b64, sizeof(key1_b64) - 1);
	assert(len == AES128_KEYLEN + 2);
	len = EVP_DecodeBlock(salt, salt_b64, sizeof(salt_b64) - 1);
	assert(len == SALT_LEN + 2);

	if (derive_prk(salt, key1, prk, errmsg) != 0) {
		fprintf(stderr, "ex1 decrypt PRK: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(prk_b64, prk, SHA256_LEN);
	assert(len == 44);
	AZ(memcmp(prk_b64, prk1_b64, len));

	if (derive_cek(prk, cek, errmsg) != 0) {
		fprintf(stderr, "ex1 decrypt CEK: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(cek_b64, cek, AES128_KEYLEN);
	assert(len == 24);
	AZ(memcmp(cek_b64, cek1_b64, len));

	memset(seq, 0, NONCE_LEN);
	if (derive_prenonce(prk, nonce, errmsg) != 0) {
		fprintf(stderr, "ex1 decrypt NONCE: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(nonce_b64, nonce, NONCE_LEN);
	assert(len == 16);
	AZ(memcmp(nonce_b64, nonce1_b64, len));

	len = EVP_DecodeBlock(body1, body1_b64, sizeof(body1_b64) - 1);
	assert(len == 54);

	AZ(memcmp(body1, salt, SALT_LEN));

	decode_header(body1, &rs, &idlen);
	assert(rs == exp_rs1);
	assert(idlen == exp_idlen1);

	if ((ctx = cipher_ctx_init(0, errmsg)) == NULL) {
		fprintf(stderr, "ex1 decrypt: cipher_ctx_init: %s\n", errmsg);
		exit(-1);
	}

	/* bodylen < rs, so we compute ciphertext_len and tag specially */
	ciphertext = body1 + HDR_PREFIX_LEN + idlen;
	len = decrypt_record(ctx, ciphertext,
	    bodylen1 - (HDR_PREFIX_LEN + idlen) - TAG_LEN,
	    body1 + (bodylen1 - TAG_LEN), cek, nonce, plaintext, &last, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex1 decrypt_record: %s\n", errmsg);
		exit(-1);
	}
	assert(len == exp_plaintext_len);
	AN(last);
	AZ(memcmp(plaintext, exp_plaintext, len));

	/* example 2, ch 3.2 */

	memset(plaintext, 0x00, sizeof(plaintext));

	len = EVP_DecodeBlock(key2, key2_b64, sizeof(key2_b64) - 1);
	assert(len == AES128_KEYLEN + 2);
	len = EVP_DecodeBlock(body2, body2_b64, sizeof(body2_b64) - 1);
	assert(len == 75);

	decode_header(body2, &rs, &idlen);
	assert(rs == exp_rs2);
	assert(idlen = exp_idlen2);
	AZ(memcmp(&body2[HDR_PREFIX_LEN], exp_keyid2, idlen));

	if (derive_prk(body2, key2, prk, errmsg) != 0) {
		fprintf(stderr, "ex2 decrypt PRK: %s\n", errmsg);
		exit(-1);
	}
	if (derive_cek(prk, cek, errmsg) != 0) {
		fprintf(stderr, "ex2 decrypt CEK: %s\n", errmsg);
		exit(-1);
	}
	if (derive_prenonce(prk, nonce, errmsg) != 0) {
		fprintf(stderr, "ex2 decrypt NONCE: %s\n", errmsg);
		exit(-1);
	}

	/* First record */
	ciphertext = body2 + HDR_PREFIX_LEN + idlen;
	len = decrypt_record(ctx, ciphertext, rs - TAG_LEN,
	    ciphertext + (rs - TAG_LEN), cek, nonce, plaintext, &last, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex2 1st record decrypt_record: %s\n", errmsg);
		exit(-1);
	}
	AZ(last);
	plaintext_len = len;

	/* Second record */
	seq[NONCE_LEN - 1] = 1; // simulates increment
	for (int i = 0; i < NONCE_LEN; i++)
		nonce[i] ^= seq[i];
	ciphertext += rs;

	len = decrypt_record(ctx, ciphertext, rs - TAG_LEN,
	    ciphertext + (rs - TAG_LEN), cek, nonce, plaintext + plaintext_len,
	    &last, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex2 2st record decrypt_record: %s\n", errmsg);
		exit(-1);
	}
	AN(last);
	plaintext_len += len;
	AZ(memcmp(plaintext, exp_plaintext, len));

	cipher_ctx_fini(ctx);

	/* example 1, ch 3.1, encryption */

	memset(plaintext, 0xff, sizeof(plaintext));
	memcpy(plaintext, exp_plaintext, exp_plaintext_len);

	/* Ordinarily call add_salt() to get random salt. */
	memcpy(body1, salt, SALT_LEN);
	encode_header(body1, exp_rs1, exp_idlen1, (unsigned char *)"");

	if (derive_prk(body1, key1, prk, errmsg) != 0) {
		fprintf(stderr, "ex1 encrypt PRK: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(prk_b64, prk, SHA256_LEN);
	assert(len == 44);
	AZ(memcmp(prk_b64, prk1_b64, len));

	if (derive_cek(prk, cek, errmsg) != 0) {
		fprintf(stderr, "ex1 encrypt CEK: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(cek_b64, cek, AES128_KEYLEN);
	assert(len == 24);
	AZ(memcmp(cek_b64, cek1_b64, len));

	seq[NONCE_LEN - 1] = 0; // simulates reset to 0
	if (derive_prenonce(prk, nonce, errmsg) != 0) {
		fprintf(stderr, "ex1 encrypt NONCE: %s\n", errmsg);
		exit(-1);
	}

	len = EVP_EncodeBlock(nonce_b64, nonce, NONCE_LEN);
	assert(len == 16);
	AZ(memcmp(nonce_b64, nonce1_b64, len));

	if ((ctx = cipher_ctx_init(1, errmsg)) == NULL) {
		fprintf(stderr, "ex1 encrypt: cipher_ctx_init: %s\n", errmsg);
		exit(-1);
	}

	/* Using a shorter record size */
	rs = 32;
	assert(rs + HDR_PREFIX_LEN + exp_idlen1 == (unsigned)bodylen1);
	last = 1;
	ciphertext = body1 + HDR_PREFIX_LEN + exp_idlen1;
	len = encrypt_record(ctx, plaintext, exp_plaintext_len, rs, cek,
	    nonce, last, ciphertext, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex1 encrypt_record: %s\n", errmsg);
		exit(-1);
	}
	assert((unsigned)len == rs);

	len = EVP_EncodeBlock(body1_test_b64, body1, bodylen1);
	assert(len == 72);
	AZ(memcmp(body1_test_b64, body1_b64, len));
	cipher_ctx_fini(ctx);

	/* example 2, ch 3.2, encryption */

	memset(plaintext, 0, sizeof(plaintext));
	memcpy(plaintext, exp_plaintext, exp_plaintext_len);

	/*
	 * The salt previously decoded for the decryption test is still in
	 * body2.
	 */
	memset(body2 + SALT_LEN, 0xff, sizeof(body2) - SALT_LEN);
	encode_header(body2, exp_rs2, exp_idlen2, (uint8_t *)exp_keyid2);

	if (derive_prk(body2, key2, prk, errmsg) != 0) {
		fprintf(stderr, "ex2 encrypt PRK: %s\n", errmsg);
		exit(-1);
	}
	if (derive_cek(prk, cek, errmsg) != 0) {
		fprintf(stderr, "ex2 encrypt CEK: %s\n", errmsg);
		exit(-1);
	}
	if (derive_prenonce(prk, nonce, errmsg) != 0) {
		fprintf(stderr, "ex2 encrypt NONCE: %s\n", errmsg);
		exit(-1);
	}

	if ((ctx = cipher_ctx_init(1, errmsg)) == NULL) {
		fprintf(stderr, "ex2 encrypt: cipher_ctx_init: %s\n", errmsg);
		exit(-1);
	}

	/* First record, first 7 bytes of the plaintext */
	last = 0;
	ciphertext = body2 + HDR_PREFIX_LEN + exp_idlen2;
	len = encrypt_record(ctx, plaintext, 7, exp_rs2, cek, nonce, last,
	    ciphertext, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex2 1st record encrypt_record: %s\n", errmsg);
		exit(-1);
	}
	assert((unsigned)len == exp_rs2);
	ciphertext_len = len;

	/* Second record, last 8 bytes of the plaintext */
	seq[NONCE_LEN - 1] = 1; // simulates increment
	for (int i = 0; i < NONCE_LEN; i++)
		nonce[i] ^= seq[i];

	ciphertext += ciphertext_len;
	last = 1;
	/* plaintext was overwritten for padding */
	memcpy(plaintext, exp_plaintext, exp_plaintext_len);
	len = encrypt_record(ctx, plaintext + 7, 8, exp_rs2, cek, nonce, last,
	    ciphertext, errmsg);
	if (len < 0) {
		fprintf(stderr, "ex1 2nd record encrypt_record: %s\n", errmsg);
		exit(-1);
	}
	assert((unsigned)len == exp_rs2);
	ciphertext_len += len;

	assert(ciphertext_len + HDR_PREFIX_LEN + exp_idlen2 == exp_bodylen2);
	len = EVP_EncodeBlock(body2_test_b64, body2, exp_bodylen2);
	assert(len == 100);
	AZ(memcmp(body2_test_b64, body2_b64, len));

	cipher_ctx_fini(ctx);

	/* Just call add_salt() for sanity's sake. */
	memset(body1, 0, SALT_LEN);
	if (add_salt(body1, errmsg) != 0) {
		fprintf(stderr, "add_salt: %s\n", errmsg);
		exit(-1);
	}

	exit(0);
}
