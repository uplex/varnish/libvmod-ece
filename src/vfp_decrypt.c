/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include "vfp_common.h"

#define ERR(ctx, msg)				\
	ERR_VFP((ctx), "decrypt", msg)

#define VERR(ctx, fmt, ...)				\
	VERR_VFP((ctx), "decrypt", fmt, __VA_ARGS__)

/* matches default fetch_chunksize */
#define DEFAULT_CHUNKSZ (16 * 1024)

#define DEFAULT_MAX_RS (1024 * 1024)

/**
 ** VFP decrypt
 **/

#define HDR_LEN(hdr) ((hdr)->next_in - (hdr)->hdr)

static enum vfp_status
decrypt_init(struct vfp_ctx *ctx, struct ece *ece, struct vfp_entry *ent)
{
	struct vfp_settings *settings;
	const struct vfp_cfg *cfg;
	struct ece_hdrbuf *hdr;
	enum vfp_status vp;
	size_t len;
	uint32_t rs;
	uint8_t idlen;
	const char *name;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->wrk, WORKER_MAGIC);
	AN(ctx->wrk->vsl);
	CHECK_OBJ_NOTNULL(ece, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->crypto, ECE_CRYPTO_MAGIC);
	CHECK_OBJ_NOTNULL(ece->hdr, ECE_HDRBUF_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);
	CHECK_OBJ_NOTNULL(cfg->settings, VFP_SETTINGS_MAGIC);
	AN(ent->vfp->name);
	settings = cfg->settings;
	name = ent->vfp->name;

	AZ(ece->rs);
	AZ(ece->chunksz);
	AZ(ece->buf);
	hdr = ece->hdr;
	AN(hdr->hdr);
	AN(hdr->next_in);
	AN(hdr->avail_in);

	if (HDR_LEN(hdr) < HDR_PFX_LEN) {
		len = HDR_PFX_LEN - HDR_LEN(hdr);
		vp = suck_bytes(ctx, hdr->next_in, &len);
		if (vp == VFP_ERROR)
			return (vp);
		hdr->next_in += len;
		assert(HDR_LEN(hdr) <= HDR_PFX_LEN);
		if (HDR_LEN(hdr) < HDR_PFX_LEN) {
			if (vp == VFP_END)
				return (ERR(ctx, "truncated header"));
			return (VFP_OK);
		}
	}

	decode_header(hdr->hdr, &rs, &idlen);
	if (rs < MIN_RS)
		return (VERR(ctx, "invalid record size %u", rs));
	/* XXX a stat should show the rs high watermark. */
	if (settings->rs != 0 && rs > settings->rs)
		return (VERR(ctx, "record size %u exceeds max %u", rs,
			     settings->rs));
	if (rs > INT_MAX)
		/*
		 * XXX This is because the input params to the libcrypto
		 * functions are typed as signed int.
		 */
		return (VERR(ctx, "record size %u may not exceed %d", rs,
			     INT_MAX));

	if (HDR_LEN(hdr) < HDR_PFX_LEN + idlen) {
		len = (HDR_PFX_LEN + idlen) - HDR_LEN(hdr);
		vp = suck_bytes(ctx, hdr->next_in, &len);
		if (vp == VFP_ERROR)
			return (vp);
		hdr->next_in += len;
		assert(HDR_LEN(hdr) <= HDR_PFX_LEN + idlen);
		if (HDR_LEN(hdr) < HDR_PFX_LEN + idlen) {
			if (vp == VFP_END)
				return (ERR(ctx, "truncated header"));
			return (VFP_OK);
		}
		assert(HDR_LEN(hdr) == HDR_PFX_LEN + idlen);
		if (vp == VFP_END)
			/* Empty decryption */
			return (VFP_NULL);
	}
	ece->rs = rs;
	assert(ece->rs >= MIN_RS);
	assert(settings->rs == 0 || ece->rs <= settings->rs);
	assert(ece->rs <= INT_MAX);

	if (crypto_init(ctx, ece->crypto, hdr->hdr, hdr->hdr + HDR_PFX_LEN,
			idlen, 0) == VFP_ERROR)
		return (VFP_ERROR);

	/* set chunksz so that the fetches fit rs */
	ece->chunksz = (settings->chunksz / ece->rs) * ece->rs;
	if (ece->chunksz == 0)
		ece->chunksz = ece->rs;
	errno = 0;
	ece->buf = malloc(ece->chunksz);
	if (ece->buf == NULL)
		return (VERR(ctx, "buffer allocation failed: %s",
			     strerror(errno)));

	errno = 0;
	ece->stream->rec_buf = malloc(ece->rs);
	if (ece->stream->rec_buf == NULL)
		return (VERR(ctx, "record buffer allocation failed: %s",
			     strerror(errno)));
	ece->stream->rec_next = ece->stream->rec_buf;
	AZ(ece->stream->rec_avail);

	VSLb(ctx->wrk->vsl, SLT_Debug, "%s: id \"%.*s\"", name, idlen,
	     hdr->hdr + HDR_PFX_LEN);
	VSLb(ctx->wrk->vsl, SLT_Debug, "%s: record size %u", name, ece->rs);
	VSLb(ctx->wrk->vsl, SLT_Debug, "%s: chunk size %zu", name,
	     ece->chunksz);

	return (VFP_END);
}

/*
 * Return VFP_ERROR on error.
 * VFP_OK if everything could be decrypted successfully up to available
 * space in the stream.
 * VFP_END if the terminating delimiter was found and we are at the
 * end of the input stream.
 * Otherwise the vp status passed in (from suck in the pull method).
 */
static enum vfp_status
decrypt(struct vfp_ctx *ctx, struct ece *ece, enum vfp_status vp)
{
	struct ece_stream *stream;
	struct ece_crypto *crypto;
	size_t record_len, ciphertext_len;
	ssize_t plaintext_len;
	int last, expect_last = 0;
	unsigned char nonce[NONCE_LEN], *tag;
	char errmsg[ERRMSG_LEN];
	uint8_t *out;
	ptrdiff_t diff;

	CHECK_OBJ_NOTNULL(ece, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->crypto, ECE_CRYPTO_MAGIC);
	CHECK_OBJ_NOTNULL(ece->stream, ECE_STREAM_MAGIC);
	stream = ece->stream;
	crypto = ece->crypto;

	while (stream->avail_in > 0) {
		record_len = ece->rs;
		if (stream->avail_in < ece->rs) {
			if (vp != VFP_END)
				return (VFP_OK);
			if (stream->avail_in < MIN_RS)
				return (ERR(ctx, "record truncated"));
			expect_last = 1;
			record_len = stream->avail_in;
		}
		assert(record_len >= MIN_RS);
		ciphertext_len = record_len - TAG_LEN;
		assert(ciphertext_len <= INT_MAX);
		tag = stream->next_in + ciphertext_len;

		out = stream->next_out;
		if (stream->avail_out < ciphertext_len)
			out = stream->rec_buf;
		/* Check the libcrypto "partially overlapping" error. */
		diff = out - stream->next_in;
		AZ(ciphertext_len > 0
		   && ((diff > 0 && diff < (ptrdiff_t)ciphertext_len)
		       || (diff < 0 && (diff > 0 - (ptrdiff_t)ciphertext_len))));

		nonce_xor_seq(crypto, nonce);
		plaintext_len = decrypt_record(ece->ectx, stream->next_in,
					       ciphertext_len, tag,
					       crypto->cek, nonce, out, &last,
					       errmsg);
		seq_inc(crypto);

		if (plaintext_len < 0)
			return (VERR(ctx, "%s", errmsg));
		if (last && vp != VFP_END)
			return (ERR(ctx, "premature last record"));
		if (!last && expect_last)
			return (ERR(ctx, "record smaller than record size"));

		stream->avail_in -= record_len;
		stream->next_in  += record_len;
		if (out == stream->rec_buf) {
			assert(plaintext_len <= ece->rs);
			stream->rec_next = stream->rec_buf;
			stream->rec_avail = plaintext_len;
			return (VFP_OK);
		}
		stream->avail_out -= plaintext_len;
		stream->next_out  += plaintext_len;

		if (stream->avail_in == 0 && !last && vp == VFP_END)
			return (ERR(ctx, "message truncated"));
		if (last) {
			if (stream->avail_in != 0 || vp != VFP_END)
				return (ERR(ctx,
					    "body data after message end"));
			return (VFP_END);
		}
	}
	return (vp);
}

enum vfp_status v_matchproto_(vfp_init_f)
vfp_decrypt_init(struct vfp_ctx *ctx, struct vfp_entry *ent)
{
	struct ece *ece;
	const struct vfp_cfg *cfg;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);

	/* XXX implement me */
	if (http_GetStatus(ctx->resp) == 206)
		return (VFP_NULL);

	if (!http_HdrIs(ctx->resp, H_Content_Encoding, "aes128gcm"))
		return (VFP_NULL);

	if (cfg->stats != NULL)
		cfg->stats->ops++;

	if (common_alloc(ctx, &ece, 0) == VFP_ERROR)
		return (VFP_ERROR);
	ent->priv1 = ece;
	AZ(ece->rs);

	http_Unset(ctx->resp, H_Content_Encoding);
	common_hdr_set(ctx);

	return (VFP_OK);
}

/*
 * RFC8188 ch 2: "The final encoding consists of a header ... and zero or
 * more fixed-size encrypted records ..."
 * So we reject an empty body, but accept a header with no records. The
 * latter results in a zero-length decryption.
 */
enum vfp_status v_matchproto_(vfp_pull_f)
vfp_decrypt_pull(struct vfp_ctx *ctx, struct vfp_entry *ent, void *ptr,
		 ssize_t *lenp)
{
	struct ece *ece;
	struct ece_stream *stream;
	struct ece_hdrbuf *hdr;
	const struct vfp_cfg *cfg;
	struct VSC_ece *stats;
	unsigned char *p = ptr, *inb4;
	enum vfp_status vp = VFP_OK;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	CAST_OBJ_NOTNULL(ece, ent->priv1, ECE_MAGIC);
	CHECK_OBJ_NOTNULL(ece->stream, ECE_STREAM_MAGIC);
	CHECK_OBJ_NOTNULL(ece->hdr, ECE_HDRBUF_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);
	AN(ptr);
	AN(lenp);
	assert(*lenp >= 0);
	stream = ece->stream;
	hdr = ece->hdr;
	stats = cfg->stats;

	if (ece->rs == 0) {
		inb4 = hdrBufIn(hdr);
		vp = decrypt_init(ctx, ece, ent);
		if (stats != NULL)
			stats->in += hdrBufIn(hdr) - inb4;
		if (vp == VFP_ERROR)
			return (vp);
		if (vp == VFP_NULL) {
			/* Empty decryption */
			*lenp = 0;
			return (VFP_END);
		}
		if (vp == VFP_OK) {
			/* Need more header */
			*lenp = 0;
			return (VFP_OK);
		}
		vp = VFP_OK;
	}
	AN(ece->rs);
	AN(ece->chunksz);
	AN(ece->buf);

	setOutputBuf(stream, ptr, *lenp);
	flushRecBuf(stream);
	if (isOutputBufFull(stream)) {
		*lenp = outputLen(stream, p);
		return (VFP_OK);
	}

	if (isInputBufEmpty(stream)) {
		size_t len = ece->chunksz;
		vp = suck_bytes(ctx, ece->buf, &len);
		assert(len <= ece->chunksz);
		if (stats != NULL)
			stats->in += len;
		if (vp == VFP_ERROR)
			return (vp);
		setInputBuf(stream, ece->buf, len);
	}
	if (!isInputBufEmpty(stream)) {
		vp = decrypt(ctx, ece, vp);
		if (vp == VFP_ERROR)
			return (vp);
	}
	else if (vp != VFP_END)
		ERR(ctx, "message truncated");

	*lenp = outputLen(stream, p);
	return (vp);
}

static struct vfp_settings default_settings = {
	.magic   = VFP_SETTINGS_MAGIC,
	.chunksz = DEFAULT_CHUNKSZ,
	.rs      = DEFAULT_MAX_RS,
};

static struct vfp_cfg default_cfg = {
	.magic = VFP_CFG_MAGIC,
	.settings = &default_settings,
	.stats = NULL,
	.vsc_seg = NULL,
};

const struct vfp vfp_decrypt = {
	.name = "ece_decrypt",
	.init = vfp_decrypt_init,
	.pull = vfp_decrypt_pull,
	.fini = vfp_common_fini,
	.priv1 = &default_cfg,
};
