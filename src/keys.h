/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdint.h>

#ifndef VRT_H_INCLUDED
#include "vrt.h"
#endif

enum tz {
	LOCAL = 0,
	UTC,
};

int KEY_Init(VRT_CTX);
void KEY_Fini(void);
void KEY_Rdlock(uint8_t idlen);
void KEY_Unlock(uint8_t idlen);
uint8_t *KEY_Get(uint8_t *id, uint8_t idlen);
int KEY_Add(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key);
int KEY_Update(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key);
int KEY_Set(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key);
void KEY_Wipe(void * const key);
int KEY_Delete(VRT_CTX, uint8_t *id, uint8_t idlen);
VCL_BOOL KEY_Exists(uint8_t *id, uint8_t idlen);
VCL_TIME KEY_Added(VRT_CTX, uint8_t *id, uint8_t idlen);
VCL_TIME KEY_Updated(VRT_CTX, uint8_t *id, uint8_t idlen);
void KEY_Dump(VRT_CTX, enum tz tz);
