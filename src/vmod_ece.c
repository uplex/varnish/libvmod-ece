/*-
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include <string.h>

#include <openssl/crypto.h>

#include "cache/cache.h"
#include "vcl.h"

#include "vcc_if.h"

#include "vfp.h"
#include "keys.h"

#define VFAIL(ctx, fmt, ...) \
        VRT_fail((ctx), "vmod ece failure: " fmt, __VA_ARGS__)

/* minimum fetch_chunksize */
#define VARNISH_MIN_CHUNKSZ 4096

struct VPFX(ece_encrypter) {
	unsigned	magic;
#define ECE_ENCRYPTER_MAGIC 0x21cb655f
	char		*vcl_name;
	struct vfp	*vfp;
	struct vfp_cfg	*cfg;
};

struct VPFX(ece_decrypter) {
	unsigned	magic;
#define ECE_DECRYPTER_MAGIC 0x2a28a833
	char		*vcl_name;
	struct vfp	*vfp;
	struct vfp_cfg	*cfg;
};

struct custom_vfp_entry {
	unsigned	magic;
#define CUSTOM_VFP_MAGIC 0xfc88cb98
	VSLIST_ENTRY(custom_vfp_entry) list;
	struct vfp	*vfp;
};

VSLIST_HEAD(custom_vfp_head, custom_vfp_entry);

/* Event function */

static struct custom_vfp_head *
init_priv_vcl(struct vmod_priv *priv)
{
	struct custom_vfp_head *vfph;

	AN(priv);
	if (priv->priv == NULL) {
		vfph = malloc(sizeof(*vfph));
		AN(vfph);
		priv->priv = vfph;
		VSLIST_INIT(vfph);
	}
	else
		vfph = priv->priv;
	return (vfph);
}

int
VPFX(event)(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	struct custom_vfp_head *vfph;
	struct custom_vfp_entry *vfpe;
	struct vfp_cfg *cfg;
	static int loaded = 0;

	ASSERT_CLI();
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(priv);

	vfph = init_priv_vcl(priv);
	AN(vfph);

	switch(e) {
	case VCL_EVENT_LOAD:
		VRT_AddVFP(ctx, &vfp_encrypt);
		VRT_AddVFP(ctx, &vfp_decrypt);

		CAST_OBJ_NOTNULL(cfg, TRUST_ME(vfp_encrypt.priv1),
				 VFP_CFG_MAGIC);
		if (cfg->stats == NULL)
			cfg->stats = VSC_ece_New(NULL, &cfg->vsc_seg,
						 "vfp.ece_encrypt");
		CAST_OBJ_NOTNULL(cfg, TRUST_ME(vfp_decrypt.priv1),
				 VFP_CFG_MAGIC);
		if (cfg->stats == NULL)
			cfg->stats = VSC_ece_New(NULL, &cfg->vsc_seg,
						 "vfp.ece_decrypt");

		assert(loaded >= 0);
		if (loaded++ == 0)
			if (KEY_Init(ctx) != 0)
				return (-1);
		return (0);
	case VCL_EVENT_DISCARD:
		VRT_RemoveVFP(ctx, &vfp_encrypt);
		VRT_RemoveVFP(ctx, &vfp_decrypt);

		while (!VSLIST_EMPTY(vfph)) {
			vfpe = VSLIST_FIRST(vfph);
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL) {
				VRT_RemoveVFP(ctx, vfpe->vfp);
				free(vfpe->vfp);
				/* cfg freed in object .fini */
			}
			VSLIST_REMOVE_HEAD(vfph, list);
			FREE_OBJ(vfpe);
		}
		free(vfph);

		AN(loaded);
		if (--loaded == 0) {
			KEY_Fini();
			CAST_OBJ_NOTNULL(cfg, TRUST_ME(vfp_encrypt.priv1),
					 VFP_CFG_MAGIC);
			if (cfg->vsc_seg != NULL)
				VSC_ece_Destroy(&cfg->vsc_seg);
			CAST_OBJ_NOTNULL(cfg, TRUST_ME(vfp_decrypt.priv1),
					 VFP_CFG_MAGIC);
			if (cfg->vsc_seg != NULL)
				VSC_ece_Destroy(&cfg->vsc_seg);
		}
		return (0);
	case VCL_EVENT_WARM:
		VSLIST_FOREACH(vfpe, vfph, list) {
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL && vfpe->vfp->priv1 != NULL) {
				CAST_OBJ(cfg, TRUST_ME(vfpe->vfp->priv1),
					 VFP_CFG_MAGIC);
				if (cfg->vsc_seg != NULL)
					VRT_VSC_Reveal(cfg->vsc_seg);
			}
		}
		return (0);
	case VCL_EVENT_COLD:
		VSLIST_FOREACH(vfpe, vfph, list) {
			CHECK_OBJ_NOTNULL(vfpe, CUSTOM_VFP_MAGIC);
			if (vfpe->vfp != NULL && vfpe->vfp->priv1 != NULL) {
				CAST_OBJ(cfg, TRUST_ME(vfpe->vfp->priv1),
					 VFP_CFG_MAGIC);
				if (cfg->vsc_seg != NULL)
					VRT_VSC_Hide(cfg->vsc_seg);
			}
		}
		return (0);
	default:
		WRONG("illegal event enum");
	}
	NEEDLESS(return (0));
}

/* Custom VFPs */

static int
crypter_init(VRT_CTX, const char *vcl_name, struct vmod_priv *priv,
	     VCL_STRING name, struct vfp **vfpp, struct vfp_cfg **cfgp)
{
	struct vfp *vfp;
	struct vfp_cfg *cfg;
	struct vfp_settings *settings;
	struct custom_vfp_head *vfph;
	struct custom_vfp_entry *vfpe;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vcl_name);
	AN(priv);
	AN(vfpp);
	AN(cfgp);

	if (name == NULL) {
		VFAIL(ctx, "new %s: filter name is NULL", vcl_name);
		return (-1);
	}

	if (*name == '\0') {
		VFAIL(ctx, "new %s: filter name must be non-empty", vcl_name);
		return (-1);
	}

	if (strcmp(name, "ece_encrypt") == 0 || strcmp(name, "ece_decrypt") == 0
	    || strcmp(name, "esi") == 0
	    || strcmp(name, "esi_gzip") == 0
	    || strcmp(name, "gunzip") == 0
	    || strcmp(name, "gzip") == 0
	    || strcmp(name, "testgunzip") == 0) {
		VFAIL(ctx,
		      "new %s: filter name %s already in use by another VFP",
		      vcl_name, name);
		return (-1);
	}

	errno = 0;
	vfp = malloc(sizeof(*vfp));
	if (vfp == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for VFP: %s",
		      vcl_name, vstrerror(errno));
		return (-1);
	}
	*vfpp = vfp;

	errno = 0;
	ALLOC_OBJ(cfg, VFP_CFG_MAGIC);
	if (cfg == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for config: %s",
		      vcl_name, vstrerror(errno));
		return (-1);
	}
	*cfgp = cfg;

	errno = 0;
	ALLOC_OBJ(settings, VFP_SETTINGS_MAGIC);
	if (settings == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for settings: %s",
		      vcl_name, vstrerror(errno));
		return (-1);
	}
	cfg->settings = settings;

	errno = 0;
	ALLOC_OBJ(vfpe, CUSTOM_VFP_MAGIC);
	if (vfpe == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for VFP list entry: %s",
		      vcl_name, vstrerror(errno));
		return (-1);
	}
	vfph = init_priv_vcl(priv);
	AN(vfph);

	vfp->name = strdup(name);
	vfp->fini = vfp_common_fini;
	vfp->priv1 = cfg;
	VRT_AddVFP(ctx, vfp);

	vfph = init_priv_vcl(priv);
	vfpe->vfp = vfp;
	VSLIST_INSERT_HEAD(vfph, vfpe, list);
	return (0);
}

static void
create_stats(VRT_CTX, const struct vfp *vfp, const char *vcl_name)
{
	struct vfp_cfg *cfg;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(vcl_name);
	if ((ctx->method & VCL_MET_INIT) == 0) {
		VFAIL(ctx, "%s.create_stats() may only be called in vcl_init",
		      vcl_name);
		return;
	}

	AN(vfp);
	CAST_OBJ_NOTNULL(cfg, TRUST_ME(vfp->priv1), VFP_CFG_MAGIC);

	cfg->stats = VSC_ece_New(NULL, &cfg->vsc_seg, "vfp.%s.%s",
				 VCL_Name(ctx->vcl), vcl_name);
	AN(cfg->stats);
	memset(cfg->stats, 0, sizeof(*cfg->stats));
}

/* Object encrypter */

VCL_VOID
vmod_encrypter__init(VRT_CTX, struct VPFX(ece_encrypter) **encp,
		     const char *vcl_name, struct vmod_priv *priv,
		     VCL_STRING name, VCL_BYTES rs, VCL_STRING key_hdr)
{
	struct VPFX(ece_encrypter) *enc;
	struct vfp *vfp;
	struct vfp_cfg *cfg;
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(encp);
	AZ(*encp);
	AN(vcl_name);
	AN(priv);

	if (key_hdr == NULL) {
		VFAIL(ctx, "new %s: key header name is NULL", vcl_name);
		return;
	}
	if (*key_hdr == '\0') {
		VFAIL(ctx, "new %s: key header name may not be empty",
		      vcl_name);
		return;
	}
	len = strlen(key_hdr);
	if (len > UINT8_MAX - 1) {
		VFAIL(ctx, "new %s: key header name %.80s too long "
		      "(length %zu > max %u)", vcl_name, key_hdr, len,
		      UINT8_MAX - 1);
		return;
	}

	/* Catches rs < 0. */
	if (rs < MIN_RS) {
		VFAIL(ctx, "new %s: rs %jd too small (must be >= %d)", vcl_name,
		      (intmax_t)rs, MIN_RS);
		return;
	}
	if (rs > INT_MAX) {
		VFAIL(ctx, "new %s: rs %jd too large (must be <= %d)", vcl_name,
		      (intmax_t)rs, INT_MAX);
		return;
	}
	/* just in case */
	if (rs > UINT32_MAX) {
		VFAIL(ctx, "new %s: rs %jd too large (must be <= %u)", vcl_name,
		      (intmax_t)rs, UINT32_MAX);
		return;
	}

	if (crypter_init(ctx, vcl_name, priv, name, &vfp, &cfg) != 0)
		return;
	AN(vfp);
	CHECK_OBJ_NOTNULL(cfg, VFP_CFG_MAGIC);
	CHECK_OBJ_NOTNULL(cfg->settings, VFP_SETTINGS_MAGIC);

	errno = 0;
	cfg->settings->key_hdr = malloc(len + 3);
	if (cfg->settings->key_hdr == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for header: %s",
		      vcl_name, vstrerror(errno));
		return;
	}
	*((uint8_t *)TRUST_ME(cfg->settings->key_hdr)) = (uint8_t)(len + 1);
	sprintf(TRUST_ME(cfg->settings->key_hdr) + 1, "%s:", key_hdr);
	cfg->settings->rs = (uint32_t)rs;

	errno = 0;
	ALLOC_OBJ(enc, ECE_ENCRYPTER_MAGIC);
	if (enc == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for object: %s",
		      vcl_name, vstrerror(errno));
		return;
	}

	vfp->init = vfp_encrypt_init;
	vfp->pull = vfp_encrypt_pull;

	enc->vfp = vfp;
	enc->vcl_name = strdup(vcl_name);
	enc->cfg = cfg;
	*encp = enc;
}

VCL_VOID
vmod_encrypter__fini(struct VPFX(ece_encrypter) **encp)
{
	struct VPFX(ece_encrypter) *enc;
	struct vfp_cfg *cfg;

	if (encp == NULL || *encp == NULL)
		return;
	TAKE_OBJ_NOTNULL(enc, encp, ECE_ENCRYPTER_MAGIC);
	if (enc->vcl_name != NULL)
		free(enc->vcl_name);
	if (enc->cfg != NULL) {
		CHECK_OBJ(enc->cfg, VFP_CFG_MAGIC);
		cfg = enc->cfg;
		if (cfg->settings != NULL) {
			CHECK_OBJ(cfg->settings, VFP_SETTINGS_MAGIC);
			if (cfg->settings->key_hdr != NULL)
				free(TRUST_ME(cfg->settings->key_hdr));
			FREE_OBJ(cfg->settings);
		}
		if (cfg->vsc_seg != NULL)
			VSC_ece_Destroy(&cfg->vsc_seg);
		FREE_OBJ(cfg);
	}
	FREE_OBJ(enc);
}

VCL_VOID
vmod_encrypter_create_stats(VRT_CTX, struct VPFX(ece_encrypter) * enc)
{
	CHECK_OBJ_NOTNULL(enc, ECE_ENCRYPTER_MAGIC);
	create_stats(ctx, enc->vfp, enc->vcl_name);
}

/* Object decrypter */

VCL_VOID
vmod_decrypter__init(VRT_CTX, struct VPFX(ece_decrypter) **decp,
		     const char *vcl_name, struct vmod_priv *priv,
		     VCL_STRING name, VCL_BYTES chunksz, VCL_BYTES max_rs)
{
	struct VPFX(ece_decrypter) *dec;
	struct vfp *vfp;
	struct vfp_cfg *cfg;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(decp);
	AZ(*decp);
	AN(vcl_name);
	AN(priv);

	/* This catches chunksz < 0 (VCL_BYTES is int64_t, hence signed). */
	if (chunksz < VARNISH_MIN_CHUNKSZ) {
		VFAIL(ctx, "new %s: chunksz %jd may not be < %d", vcl_name,
		      (intmax_t)chunksz, VARNISH_MIN_CHUNKSZ);
		return;
	}
	/* chunksz cannot be too large when cast to size_t */
	assert(INT64_MAX <= SIZE_MAX);

	if (max_rs != 0) {
		/* Catches max_rs < 0. */
		if (max_rs < MIN_RS) {
			VFAIL(ctx, "new %s: max_rs %jd too small "
			      "(must be 0 or >= %d)", vcl_name,
			      (intmax_t)max_rs, MIN_RS);
			return;
		}
		if (max_rs > INT_MAX) {
			VFAIL(ctx, "new %s: max_rs %jd too large "
			      "(must be <= %d)", vcl_name, (intmax_t)max_rs,
			      INT_MAX);
			return;
		}
		/* just in case INT_MAX >= UINT32_MAX */
		if (max_rs > UINT32_MAX) {
			VFAIL(ctx, "new %s: max_rs %jd too large "
			      "(must be <= %u)", vcl_name, (intmax_t)max_rs,
			      UINT32_MAX);
			return;
		}
	}

	if (crypter_init(ctx, vcl_name, priv, name, &vfp, &cfg) != 0)
		return;
	AN(vfp);
	CHECK_OBJ_NOTNULL(cfg, VFP_CFG_MAGIC);
	CHECK_OBJ_NOTNULL(cfg->settings, VFP_SETTINGS_MAGIC);

	errno = 0;
	ALLOC_OBJ(dec, ECE_DECRYPTER_MAGIC);
	if (dec == NULL) {
		VFAIL(ctx, "new %s: cannot allocate space for object: %s",
		      vcl_name, strerror(errno));
		return;
	}

	cfg->settings->chunksz = (size_t)chunksz;
	cfg->settings->rs = (uint32_t)max_rs;

	vfp->init = vfp_decrypt_init;
	vfp->pull = vfp_decrypt_pull;

	dec->vfp = vfp;
	dec->vcl_name = strdup(vcl_name);
	dec->cfg = cfg;
	*decp = dec;
}

/*
 * The settings and custom VFP objects and PRIV_VCL list entry are freed
 * on the DISCARD event, because we need a VRT_CTX to call
 * VRT_RemoveVFP().
 */
VCL_VOID vmod_decrypter__fini(struct VPFX(ece_decrypter) **decp)
{
	struct VPFX(ece_decrypter) *dec;
	struct vfp_cfg *cfg;

	if (decp == NULL || *decp == NULL)
		return;
	TAKE_OBJ_NOTNULL(dec, decp, ECE_DECRYPTER_MAGIC);
	if (dec->vcl_name != NULL)
		free(dec->vcl_name);
	if (dec->cfg != NULL) {
		CHECK_OBJ(dec->cfg, VFP_CFG_MAGIC);
		cfg = dec->cfg;
		if (cfg->settings != NULL) {
			CHECK_OBJ(cfg->settings, VFP_SETTINGS_MAGIC);
			FREE_OBJ(cfg->settings);
		}
		if (cfg->vsc_seg != NULL)
			VSC_ece_Destroy(&cfg->vsc_seg);
		FREE_OBJ(cfg);
	}
	FREE_OBJ(dec);
}

VCL_VOID
vmod_decrypter_create_stats(VRT_CTX, struct VPFX(ece_decrypter) * dec)
{
	CHECK_OBJ_NOTNULL(dec, ECE_DECRYPTER_MAGIC);
	create_stats(ctx, dec->vfp, dec->vcl_name);
}

/* Key manipulation functions */

#define CHECK_ID(ctx, id, len, ret)					\
	do {								\
		if ((id) == NULL) {					\
			VRT_fail((ctx), "key id is NULL");		\
			return ret;					\
		}							\
		len = strlen(id);					\
		if ((len) > 255) {					\
			VRT_fail((ctx), "key id \"%.80s...\" too long "	\
				 "(length %zu > 255)", (id), (len));	\
			return ret;					\
		}							\
	} while(0)

#define CHECK_KEY(ctx, key)						\
	do {								\
		if ((key) == NULL || (key)->blob == NULL) {		\
			VRT_fail((ctx), "key contents are empty");	\
			return;						\
		}							\
		if ((key)->len != 16) {					\
			VRT_fail((ctx), "illegal key length %zu (must be 16)", \
				 (key)->len);				\
			return;						\
		}							\
	} while(0)

VCL_VOID
vmod_add_key(VRT_CTX, VCL_STRING id, VCL_BLOB key)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	CHECK_ID(ctx, id, len,);
	CHECK_KEY(ctx, key);

	/* KEY_Add calls VRT_fail() on error. */
	(void)KEY_Add(ctx, (uint8_t *)id, (uint8_t)len, key->blob);
}

VCL_VOID
vmod_update_key(VRT_CTX, VCL_STRING id, VCL_BLOB key)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	CHECK_ID(ctx, id, len,);
	CHECK_KEY(ctx, key);

	/* KEY_Update calls VRT_fail() on error. */
	(void)KEY_Update(ctx, (uint8_t *)id, (uint8_t)len, key->blob);
}

VCL_VOID
vmod_set_key(VRT_CTX, VCL_STRING id, VCL_BLOB key)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_ID(ctx, id, len,);
	CHECK_KEY(ctx, key);

	/* KEY_Set calls VRT_fail() on error. */
	(void)KEY_Set(ctx, (uint8_t *)id, (uint8_t)len, key->blob);
}

VCL_VOID
vmod_delete_key(VRT_CTX, VCL_STRING id)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_ID(ctx, id, len,);

	/* KEY_Delete calls VRT_fail() on error. */
	(void)KEY_Delete(ctx, (uint8_t *)id, (uint8_t)len);
}

VCL_BOOL
vmod_key_exists(VRT_CTX, VCL_STRING id)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_ID(ctx, id, len, 0);

	return (KEY_Exists((uint8_t *)id, (uint8_t)len));
}

VCL_TIME
vmod_key_added(VRT_CTX, VCL_STRING id)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_ID(ctx, id, len, 0);

	return (KEY_Added(ctx, (uint8_t *)id, (uint8_t)len));
}

VCL_TIME
vmod_key_updated(VRT_CTX, VCL_STRING id)
{
	size_t len;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_ID(ctx, id, len, 0);

	return (KEY_Updated(ctx, (uint8_t *)id, (uint8_t)len));
}

VCL_VOID
vmod_dump_keys(VRT_CTX, VCL_ENUM zone)
{
	enum tz tz = LOCAL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if ((ctx->method & VCL_MET_SYNTH) == 0) {
		VRT_fail(ctx, "dump_keys() may only be called in vcl_synth");
		return;
	}

	if (zone == VENUM(UTC))
		tz = UTC;
	KEY_Dump(ctx, tz);
}

/* Version functions */

VCL_STRING
vmod_libcrypto_version(VRT_CTX)
{
	(void)ctx;
	VCL_STRING version = OpenSSL_version(OPENSSL_VERSION);
	if (version == NULL)
		return "unknown libcrypto version";
	return version;
}

VCL_STRING
vmod_version(VRT_CTX)
{
	(void) ctx;
	return VERSION;
}
