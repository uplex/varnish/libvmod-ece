/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include "cache/cache.h"

#include <string.h>

#include "rfc8188.h"
#include "vfp.h"

#define HDR_PFX_LEN (SALT_LEN + 4 + 1)
#define MAX_ID_LEN UINT8_MAX

#define ERR_VFP(ctx, type, msg)			\
	VFP_Error((ctx), "ece " type ": " msg)

#define VERR_VFP(ctx, type, fmt, ...)				\
	VFP_Error((ctx), "ece " type ": " fmt, __VA_ARGS__)

struct ece_stream {
	unsigned		magic;
#define ECE_STREAM_MAGIC	0x9ea68cef
	uint8_t			*rec_buf;
	uint8_t			*rec_next;
	uint8_t			*next_in;
	uint8_t			*next_out;
	size_t			avail_in;
	size_t			avail_out;
	size_t			rec_avail;
};

struct ece_hdrbuf {
	unsigned		magic;
#define ECE_HDRBUF_MAGIC	0xed9a00cf
	uint8_t			hdr[HDR_PFX_LEN + MAX_ID_LEN];
	uint8_t			*next_in;
	uint16_t		avail_in;
};

struct ece_crypto {
	unsigned		magic;
#define ECE_CRYPTO_MAGIC	0xe7f66e91
	unsigned char		cek[AES128_KEYLEN];
	uint64_t		seq_lo;
	uint64_t		prenonce_lo;
	uint32_t		seq_hi;
	uint32_t		prenonce_hi;
};

struct ece {
	unsigned		magic;
#define ECE_MAGIC		0x2b066cec
	struct ece_crypto	*crypto;
	struct ece_stream	*stream;
	struct ece_hdrbuf	*hdr;
	EVP_CIPHER_CTX		*ectx;
	uint8_t			*buf;
	size_t			chunksz;
	uint32_t		rs;
};

enum vfp_status suck_bytes(struct vfp_ctx *ctx, void *ptr, size_t *lenp);

enum vfp_status crypto_init(struct vfp_ctx *ctx, struct ece_crypto *crypto,
			    uint8_t *salt, uint8_t *id, uint8_t idlen, int enc);

enum vfp_status common_alloc(struct vfp_ctx *ctx, struct ece **ecep, int enc);

static inline void
setInputBuf(struct ece_stream *stream, void *ptr, ssize_t len)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	AZ(stream->avail_in);
	stream->next_in = ptr;
	stream->avail_in = len;
}

static inline int
isInputBufEmpty(struct ece_stream *stream)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	return (stream->avail_in == 0);
}

static inline enum vfp_status
fillInputBuf(struct vfp_ctx *ctx, struct ece_stream *stream, size_t chunksz,
	     enum vfp_status vp)
{
	size_t len;
	enum vfp_status v;

	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	if (stream->avail_in >= chunksz)
		return (vp);
	len = chunksz - stream->avail_in;
	v = suck_bytes(ctx, stream->next_in, &len);
	assert(len <= chunksz - stream->avail_in);
	stream->next_in  += len;
	stream->avail_in -= len;
	return (v);
}

static inline void
setOutputBuf(struct ece_stream *stream, void *ptr, ssize_t len)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	stream->next_out = ptr;
	stream->avail_out = len;
}

static inline int
isOutputBufFull(struct ece_stream *stream)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	return (stream->avail_out == 0);
}

static inline void
flushRecBuf(struct ece_stream *stream)
{
	size_t len;

	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	if (stream->rec_avail == 0)
		return;
	len = stream->rec_avail;
	if (len > stream->avail_out)
		len = stream->avail_out;
	memcpy(stream->next_out, stream->rec_next, len);
	stream->next_out  += len;
	stream->avail_out -= len;
	stream->rec_next  += len;
	stream->rec_avail -= len;
}

static inline int
isHdrBufEmpty(struct ece_hdrbuf *hdrbuf)
{
	CHECK_OBJ_NOTNULL(hdrbuf, ECE_HDRBUF_MAGIC);
	return (hdrbuf->avail_in == 0);
}

static inline void
flushHdrBuf(struct ece_stream *stream, struct ece_hdrbuf *hdrbuf)
{
	size_t len;

	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	CHECK_OBJ_NOTNULL(hdrbuf, ECE_HDRBUF_MAGIC);

	len = hdrbuf->avail_in;
	if (len > stream->avail_out)
		len = stream->avail_out;
	memcpy(stream->next_out, hdrbuf->next_in, len);
	stream->next_out  += len;
	stream->avail_out -= len;
	hdrbuf->next_in   += len;
	hdrbuf->avail_in  -= len;
}

static inline ssize_t
outputLen(struct ece_stream *stream, unsigned char *ptr)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	assert(stream->next_out >= ptr);
	return (stream->next_out - ptr);
}

static inline uint8_t *
hdrBufIn(struct ece_hdrbuf *hdr)
{
	CHECK_OBJ_NOTNULL(hdr, ECE_HDRBUF_MAGIC);
	return (hdr->next_in);
}

static inline uint8_t *
streamIn(struct ece_stream *stream)
{
	CHECK_OBJ_NOTNULL(stream, ECE_STREAM_MAGIC);
	return (stream->next_in);
}

static inline void
seq_inc(struct ece_crypto *crypto)
{
	CHECK_OBJ_NOTNULL(crypto, ECE_CRYPTO_MAGIC);

	if (crypto->seq_lo < UINT64_MAX)
		crypto->seq_lo++;
	else {
		assert(crypto->seq_hi != UINT32_MAX);
		crypto->seq_hi++;
		crypto->seq_lo = 0;
	}
}

static inline void
nonce_xor_seq(struct ece_crypto *crypto, uint8_t *nonce)
{
	uint32_t nonce_hi;
	uint64_t nonce_lo;

	CHECK_OBJ_NOTNULL(crypto, ECE_CRYPTO_MAGIC);
	AN(nonce);

	nonce_hi = crypto->prenonce_hi ^ crypto->seq_hi;
	nonce_lo = crypto->prenonce_lo ^ crypto->seq_lo;

	vbe32enc(nonce, nonce_hi);
	vbe64enc(nonce + 4, nonce_lo);
}

static inline void
common_hdr_set(struct vfp_ctx *ctx)
{
	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);

	http_Unset(ctx->resp, H_Content_Length);
	RFC2616_Weaken_Etag(ctx->resp);
	ctx->obj_flags |= OF_CHGCE;
}
