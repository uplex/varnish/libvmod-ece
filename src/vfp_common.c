/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include "vfp_common.h"
#include "keys.h"

static const char *which[] = { "decrypt", "encrypt" };

enum vfp_status
suck_bytes(struct vfp_ctx *ctx, void *ptr, size_t *lenp)
{
	size_t len, off;
	ssize_t l;
	enum vfp_status vp = VFP_OK;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	AN(ptr);
	AN(lenp);
	len = *lenp;
	assert(len > 0);

	for (off = 0, l = len; vp == VFP_OK && off < len; l = len - off) {
		vp = VFP_Suck(ctx, ptr + off, &l);
		assert(l >= 0);
		assert(vp >= VFP_ERROR && vp < VFP_NULL);
		if (vp == VFP_ERROR)
			return (vp);
		if (l == 0)
			break;
		off += l;
		assert((unsigned)off <= len);
	}
	assert(off <= len);
	*lenp = off;
	return (vp);
}

enum vfp_status
crypto_init(struct vfp_ctx *ctx, struct ece_crypto *crypto, uint8_t *salt,
	    uint8_t *id, uint8_t idlen, int enc)
{
	enum vfp_status vp = VFP_OK;
	uint8_t *key, prk[SHA256_LEN], cek[SHA256_LEN], prenonce[NONCE_LEN];
	char errmsg[ERRMSG_LEN];

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(crypto, ECE_CRYPTO_MAGIC);
	AN(salt);
	AN(id);
	AZ(crypto->seq_hi);
	AZ(crypto->seq_lo);

	KEY_Rdlock(idlen);
	key = KEY_Get(id, idlen);
	if (key == NULL)
		vp = VFP_Error(ctx, "ece %s failure: unknown key %.*s",
			       which[enc], idlen, id);
	else if (derive_prk(salt, key, prk, errmsg) != 0)
		vp = VFP_Error(ctx, "ece %s failure: %s", which[enc], errmsg);
	KEY_Unlock(idlen);

	if (vp == VFP_ERROR)
		return (vp);
	if (derive_cek(prk, cek, errmsg) != 0)
		return (VFP_Error(ctx, "ece %s failure: %s", which[enc],
				  errmsg));
	memcpy(crypto->cek, cek, AES128_KEYLEN);
	if (derive_prenonce(prk, prenonce, errmsg) != 0)
		return (VFP_Error(ctx, "ece %s failure: %s", which[enc],
				  errmsg));

	KEY_Wipe(prk);
	KEY_Wipe(prk + AES128_KEYLEN);
	crypto->prenonce_hi = vbe32dec(prenonce);
	crypto->prenonce_lo = vbe64dec(prenonce + 4);
	return (VFP_OK);
}

enum vfp_status
common_alloc(struct vfp_ctx *ctx, struct ece **ecep, int enc)
{
	struct ece *ece;
	struct ece_crypto *crypto;
	struct ece_stream *stream;
	struct ece_hdrbuf *hdrbuf;
	char errmsg[ERRMSG_LEN];

	errno = 0;
	ALLOC_OBJ(ece, ECE_MAGIC);
	if (ece == NULL)
		return (VFP_Error(ctx, "ece %s failure: allocation in init: %s",
				  which[enc], strerror(errno)));

	ece->ectx = cipher_ctx_init(enc, errmsg);
	if (ece->ectx == NULL)
		return (VFP_Error(ctx, "ece %s failure: "
				  "initializing cipher context: %s", which[enc],
				  errmsg));

	errno = 0;
	ALLOC_OBJ(crypto, ECE_CRYPTO_MAGIC);
	if (crypto == NULL)
		return (VFP_Error(ctx, "ece %s failure: allocation in init: %s",
				  which[enc], strerror(errno)));

	ALLOC_OBJ(stream, ECE_STREAM_MAGIC);
	if (crypto == NULL)
		return (VFP_Error(ctx, "ece %s failure: allocation in init: %s",
				  which[enc], strerror(errno)));


	errno = 0;
	ALLOC_OBJ(hdrbuf, ECE_HDRBUF_MAGIC);
	if (hdrbuf == NULL)
		return (VFP_Error(ctx, "ece %s failure: "
				  "allocation in init: %s",
				  which[enc], strerror(errno)));
	hdrbuf->next_in = hdrbuf->hdr;
	hdrbuf->avail_in = HDR_PFX_LEN + MAX_ID_LEN;

	ece->crypto = crypto;
	ece->stream = stream;
	ece->hdr = hdrbuf;

	*ecep = ece;
	return (VFP_OK);
}

/* VFP .fini method for both encrypt and decrypt */

void v_matchproto_(vfp_fini_f)
vfp_common_fini(struct vfp_ctx *ctx, struct vfp_entry *ent)
{
	struct ece *ece;
	const struct vfp_cfg *cfg;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ent, VFP_ENTRY_MAGIC);
	AN(ent->vfp);
	CAST_OBJ_NOTNULL(cfg, ent->vfp->priv1, VFP_CFG_MAGIC);

	if (cfg->stats != NULL) {
		cfg->stats->out += ent->bytes_out;
		cfg->stats->calls += ent->calls;
		if (ent->closed == VFP_ERROR)
			cfg->stats->errs++;
	}

	if (ent->priv1 == NULL)
		return;

	CAST_OBJ(ece, ent->priv1, ECE_MAGIC);
	if (ece->ectx != NULL)
		cipher_ctx_fini(ece->ectx);
	if (ece->buf != NULL)
		free(ece->buf);
	if (ece->crypto != NULL) {
		CHECK_OBJ(ece->crypto, ECE_CRYPTO_MAGIC);
		KEY_Wipe(ece->crypto->cek);
		FREE_OBJ(ece->crypto);
	}
	if (ece->stream != NULL) {
		CHECK_OBJ(ece->stream, ECE_STREAM_MAGIC);
		if (ece->stream->rec_buf != NULL)
			free(ece->stream->rec_buf);
		FREE_OBJ(ece->stream);
	}
	if (ece->hdr != NULL) {
		CHECK_OBJ(ece->hdr, ECE_HDRBUF_MAGIC);
		FREE_OBJ(ece->hdr);
	}
	FREE_OBJ(ece);
}
