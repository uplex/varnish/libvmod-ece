/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

/* Used only if set-salt was enabled at configure time. */
#ifdef ENABLE_SET_SALT

#include <stdint.h>
#include <string.h>

#include "rfc8188.h"

#include "cache/cache.h"
#include "cache/cache_filter.h"

/*
 * libcrypto base64 encodings use the alphabet [a-ZA-Z0-9/+] and = padding
 * (just like BASE64 for VMOD blob).
 *
 * EVP_DecodeBlock() adds padding bytes, so that
 *	len(decoded) == len(encoded)*3/4
 * hence the use of SALT_LEN + 2.
 */

#define SET_SALT_HDR "\017XYZZY-ECE-Salt:"
#define SALT_B64_LEN 24
#define SALT_DECODED_LEN (SALT_LEN + 2)

enum vfp_status
set_salt(struct vfp_ctx *ctx, uint8_t *salt)
{
	const char *salt_b64;
	uint8_t decoded[SALT_DECODED_LEN];
	int len;

	CHECK_OBJ_NOTNULL(ctx, VFP_CTX_MAGIC);
	AN(salt);

	if (http_GetHdr(ctx->req, SET_SALT_HDR, &salt_b64) == 0)
		return (VFP_NULL);

	len = EVP_DecodeBlock(decoded, (const unsigned char *)salt_b64,
			      SALT_B64_LEN);
	if (len != SALT_DECODED_LEN)
		return (VFP_Error(ctx, "set-salt base64 decoding error"));

	memcpy(salt, decoded, SALT_LEN);
	return (VFP_OK);
}

#endif // ENABLE_SET_SALT
