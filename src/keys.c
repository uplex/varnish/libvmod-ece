/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

// for pthread_rwlock_* (including _setkind_np) and posix_memalign()
#define _POSIX_C_SOURCE 200809L

#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

#include "cache/cache.h"
#include "vtim.h"
#include "vsb.h"
#include "verrno.h"

#include "keys.h"
#include "rfc8188.h"

struct key {
	unsigned	magic;
#define KEY_MAGIC 0xb4f7d1eb
	VRBT_ENTRY(key)	entry;
	uint8_t		*key;
	uint8_t		*id;
	vtim_real	added;
	vtim_real	updated;
	uint8_t		idlen;
};

static inline int
key_cmp(const struct key *k1, const struct key *k2)
{
	assert(k1->idlen == k2->idlen);
	return (memcmp(k1->id, k2->id, k1->idlen));
}

VRBT_HEAD(key_tree, key);

VRBT_PROTOTYPE_STATIC(key_tree, key, entry, key_cmp);
VRBT_GENERATE_STATIC(key_tree, key, entry, key_cmp);

struct key_ent {
	struct key_tree		tree;
	pthread_rwlock_t	lock;
};

static struct key_ent key_tbl[UINT8_MAX + 1];

static long pagesz = 0;
static struct VSC_lck *lck_page;
static struct vsc_seg *lck_page_vsc_seg;
static struct lock page_mtx;

/* List of allocated key pages */
struct page_ent {
	unsigned		magic;
#define PAGE_ENTRY_MAGIC 0xde66bce4
	VTAILQ_ENTRY(page_ent)	list;
	void			*addr;
};

VTAILQ_HEAD(page_head_s, page_ent) page_head
	= VTAILQ_HEAD_INITIALIZER(page_head);

/* Free list of key addresses */
struct addr_ent {
	unsigned		magic;
#define ADDR_ENTRY_MAGIC 0xde66bce4
	VSTAILQ_ENTRY(addr_ent)	list;
	uint8_t			*addr;
};

VSTAILQ_HEAD(addr_head_s, addr_ent) addr_head
	= VSTAILQ_HEAD_INITIALIZER(addr_head);

/* allocate a page, mlock it, fill the free list with addresses */
/* XXX the number of pages in use only increases, never decreases */
static int
key_alloc_page(VRT_CTX)
{
	void *page, *p;
	struct page_ent *page_ent;
	struct addr_ent *addr_ent;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(pagesz);

	Lck_AssertHeld(&page_mtx);
	errno = 0;
	if (posix_memalign(&page, pagesz, pagesz) != 0) {
		VRT_fail(ctx, "allocating space for keys: %s",
			 vstrerror(errno));
		return (-1);
	}

	errno = 0;
	if (mlock(page, pagesz) != 0) {
		VRT_fail(ctx, "cannot lock memory for keys (mlock(2)): %s",
			 vstrerror(errno));
		return (-1);
	}

	errno = 0;
	ALLOC_OBJ(page_ent, PAGE_ENTRY_MAGIC);
	if (page_ent == NULL) {
		VRT_fail(ctx, "allocating page entry: %s", vstrerror(errno));
		return (-1);
	}
	page_ent->addr = page;
	VTAILQ_INSERT_HEAD(&page_head, page_ent, list);

	for (p = page; p < page + pagesz; p += AES128_KEYLEN) {
		errno = 0;
		ALLOC_OBJ(addr_ent, ADDR_ENTRY_MAGIC);
		if (addr_ent == NULL) {
			VRT_fail(ctx, "allocating address entry: %s",
				 vstrerror(errno));
			return (-1);
		}
		addr_ent->addr = p;
		VSTAILQ_INSERT_TAIL(&addr_head, addr_ent, list);
	}
	return (0);
}

int
KEY_Init(VRT_CTX)
{
	int ret;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

#ifdef HAVE_PTHREAD_RWLOCKATTR_SETKIND_NP
	/* If we can, tell pthread_rwlock to prevent writer starvation. */
	pthread_rwlockattr_t lock_attr;
	AZ(pthread_rwlockattr_init(&lock_attr));
	AZ(pthread_rwlockattr_setkind_np(&lock_attr,
				 PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP));
# define LOCK_ATTR (&lock_attr)
#else
# define LOCK_ATTR (NULL)
#endif

	errno = 0;
	pagesz = sysconf(_SC_PAGESIZE);
	if (pagesz < 1) {
		VRT_fail(ctx, "cannot determine page size: %s",
			 vstrerror(errno));
		return (-1);
	}
	/* requirements for posix_memalign() */
	if ((pagesz & (pagesz - 1)) != 0) {
		VRT_fail(ctx, "page size %ld is not a power of two", pagesz);
		return (-1);
	}
	if ((pagesz % sizeof(void *)) != 0) {
		VRT_fail(ctx, "page size %ld is not a multiple of address size "
			 "%zu", pagesz, sizeof(void *));
		return (-1);
	}

	lck_page = Lck_CreateClass(&lck_page_vsc_seg, "ece.key_mem");
	AN(lck_page);
	Lck_New(&page_mtx, lck_page);

	Lck_Lock(&page_mtx);
	ret = key_alloc_page(ctx);
	Lck_Unlock(&page_mtx);
	if (ret != 0)
		return (-1);

	for (unsigned i = 0; i < UINT8_MAX; i++) {
		VRBT_INIT(&key_tbl[i].tree);
		AZ(pthread_rwlock_init(&key_tbl[i].lock, LOCK_ATTR));
	}
	return (0);
}

static inline void
wipe(void * const dst, size_t len, uint8_t val)
{
	volatile uint8_t *p = (volatile uint8_t *)dst;

	while (((uintptr_t)p & (sizeof(uint64_t)-1)) && len) {
		*p++ = val;
		len--;
	}
	if (len >= sizeof(uint64_t)) {
		volatile uint64_t *p64 = (volatile void *)p;
		uint64_t val64 = (uint64_t)0x0101010101010101 * val;
		do {
			*p64++ = val64;
			len -= sizeof(uint64_t);
		} while (len >= sizeof(uint64_t));
		p = (volatile void *)p64;
	}
	while (len) {
		*p++ = val;
		len--;
	}
}

void
KEY_Wipe(void * const key)
{
	AN(key);
	wipe(key, AES128_KEYLEN, 0xff);
	wipe(key, AES128_KEYLEN, 0xaa);
	wipe(key, AES128_KEYLEN, 0x55);
	wipe(key, AES128_KEYLEN, 0x00);
}

/*
 * wipe all the keys, destroy the rwlocks, de-allocate the free list,
 * de-allocate the key pages
 */
void
KEY_Fini(void)
{
	struct key_tree *tree_h;
	struct key *key, *nxt_k;
	struct addr_ent *addr, *nxt_addr;
	struct page_ent *page, *nxt_page;

	for (unsigned i = 0; i < UINT8_MAX; i++) {
		AZ(pthread_rwlock_destroy(&key_tbl[i].lock));

		tree_h = &key_tbl[i].tree;
		if (!VRBT_EMPTY(tree_h)) {
			key = VRBT_ROOT(tree_h);
			while (key != NULL) {
				CHECK_OBJ(key, KEY_MAGIC);
				AN(key->key);
				KEY_Wipe(key->key);
				AN(key->id);
				free(key->id);
				nxt_k = VRBT_NEXT(key_tree, tree_h, key);
				VRBT_REMOVE(key_tree, tree_h, key);
				FREE_OBJ(key);
				key = nxt_k;
			}
		}
	}
	if (!VSTAILQ_EMPTY(&addr_head)) {
		addr = VSTAILQ_FIRST(&addr_head);
		while (addr != NULL) {
			CHECK_OBJ(addr, ADDR_ENTRY_MAGIC);
			nxt_addr = VSTAILQ_NEXT(addr, list);
			VSTAILQ_REMOVE_HEAD(&addr_head, list);
			FREE_OBJ(addr);
			addr = nxt_addr;
		}
	}
	if (!VTAILQ_EMPTY(&page_head)) {
		page = VTAILQ_FIRST(&page_head);
		while (page != NULL) {
			CHECK_OBJ(page, PAGE_ENTRY_MAGIC);
			AN(page->addr);
			free(page->addr);
			nxt_page = VTAILQ_NEXT(page, list);
			VTAILQ_REMOVE(&page_head, page, list);
			FREE_OBJ(page);
			page = nxt_page;
		}
	}
	Lck_Delete(&page_mtx);
	Lck_DestroyClass(&lck_page_vsc_seg);
}

void
KEY_Rdlock(uint8_t idlen)
{
	AZ(pthread_rwlock_rdlock(&key_tbl[idlen].lock));
}

void
KEY_Unlock(uint8_t idlen)
{
	AZ(pthread_rwlock_unlock(&key_tbl[idlen].lock));
}

static inline void
key_wrlock(uint8_t idlen)
{
	AZ(pthread_rwlock_wrlock(&key_tbl[idlen].lock));
}

static inline struct key *
key_find(struct key_tree *tree_h, uint8_t *id, uint8_t idlen)
{
	struct key *key;
	struct key keycmp;

	AN(tree_h);
	if (VRBT_EMPTY(tree_h))
		return NULL;
	keycmp.id = id;
	keycmp.idlen = idlen;
	key = VRBT_FIND(key_tree, tree_h, &keycmp);
	CHECK_OBJ_ORNULL(key, KEY_MAGIC);
	if (key != NULL) {
		AN(key->key);
		AN(key->id);
		AN(key->added);
		AN(key->updated);
		assert(key->idlen == idlen);
		AZ(memcmp(key->id, id, idlen));
	}
	return (key);
}

uint8_t *
KEY_Get(uint8_t *id, uint8_t idlen)
{
	struct key_tree *tree_h;
	struct key *key;

	AN(id);
	tree_h = &key_tbl[idlen].tree;
	key = key_find(tree_h, id, idlen);
	if (key == NULL)
		return (NULL);
	CHECK_OBJ(key, KEY_MAGIC);
	AN(key->key);
	AN(key->id);
	AN(key->added);
	AN(key->updated);
	assert(key->idlen == idlen);
	AZ(memcmp(key->id, id, idlen));
	return (key->key);
}

/* MUST be called under the writer lock */
static struct key *
key_insert(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key,
	   struct key_tree *tree_h)
{
	struct key *k;
	struct addr_ent *addr_ent;

	errno = 0;
	ALLOC_OBJ(k, KEY_MAGIC);
	if (k == NULL) {
		VRT_fail(ctx, "cannot allocate key entry: %s",
			 vstrerror(errno));
		return NULL;
	}

	errno = 0;
	k->id = malloc(idlen);
	if (k->id == NULL) {
		VRT_fail(ctx, "cannot allocate key id: %s", vstrerror(errno));
		FREE_OBJ(k);
		return NULL;
	}

	Lck_Lock(&page_mtx);
	if (VSTAILQ_EMPTY(&addr_head))
		if (key_alloc_page(ctx) != 0) {
			Lck_Unlock(&page_mtx);
			free(k->id);
			FREE_OBJ(k);
			return NULL;
		}
	assert(!VSTAILQ_EMPTY(&addr_head));
	addr_ent = VSTAILQ_FIRST(&addr_head);
	CHECK_OBJ_NOTNULL(addr_ent, ADDR_ENTRY_MAGIC);
	AN(addr_ent->addr);
	VSTAILQ_REMOVE_HEAD(&addr_head, list);
	Lck_Unlock(&page_mtx);

	k->key = addr_ent->addr;
	FREE_OBJ(addr_ent);
	memcpy(k->key, key, AES128_KEYLEN);
	memcpy(k->id, id, idlen);
	k->idlen = idlen;
	k->added = k->updated = ctx->now;
	AZ(VRBT_INSERT(key_tree, tree_h, k));
	return (k);
}

int
KEY_Add(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key)
{
	struct key_tree *tree_h;
	struct key *k;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(id);
	AN(key);

	key_wrlock(idlen);

	tree_h = &key_tbl[idlen].tree;
	k = key_find(tree_h, id, idlen);
	if (k != NULL) {
		VRT_fail(ctx, "key \"%.*s\" already exists", idlen, id);
		KEY_Unlock(idlen);
		return (-1);
	}
	if ((k = key_insert(ctx, id, idlen, key, tree_h)) == NULL) {
		KEY_Unlock(idlen);
		return (-1);
	}
	KEY_Unlock(idlen);

	CHECK_OBJ(k, KEY_MAGIC);
	AN(k->key);
	AN(k->id);
	AN(k->added);
	AN(k->updated);
	AZ(memcmp(k->key, key, AES128_KEYLEN));
	assert(k->idlen == idlen);
	AZ(memcmp(k->id, id, idlen));
	return (0);
}

int
KEY_Set(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key)
{
	struct key_tree *tree_h;
	struct key *k;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(id);
	AN(key);

	key_wrlock(idlen);

	tree_h = &key_tbl[idlen].tree;
	k = key_find(tree_h, id, idlen);
	if (k == NULL) {
		k = key_insert(ctx, id, idlen, key, tree_h);
		if (k == NULL) {
			KEY_Unlock(idlen);
			return (-1);
		}
	}
	CHECK_OBJ(k, KEY_MAGIC);
	AN(k->key);
	memcpy(k->key, key, AES128_KEYLEN);
	k->updated = ctx->now;
	KEY_Unlock(idlen);

	AN(k->added);
	AN(k->id);
	assert(k->idlen == idlen);
	AZ(memcmp(k->id, id, idlen));
	return (0);
}

int
KEY_Update(VRT_CTX, uint8_t *id, uint8_t idlen, const uint8_t *key)
{
	struct key_tree *tree_h;
	struct key *k;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(id);
	AN(key);

	key_wrlock(idlen);

	tree_h = &key_tbl[idlen].tree;
	k = key_find(tree_h, id, idlen);
	if (k == NULL) {
		KEY_Unlock(idlen);
		VRT_fail(ctx, "key \"%.*s\" does not exist", idlen, id);
		return (-1);
	}
	CHECK_OBJ(k, KEY_MAGIC);
	AN(k->key);
	memcpy(k->key, key, AES128_KEYLEN);
	k->updated = ctx->now;
	KEY_Unlock(idlen);

	AN(k->added);
	AN(k->id);
	assert(k->idlen == idlen);
	AZ(memcmp(k->id, id, idlen));
	return (0);
}

int
KEY_Delete(VRT_CTX, uint8_t *id, uint8_t idlen)
{
	struct key_tree *tree_h;
	struct key *k;
	struct addr_ent *addr_ent;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(id);

	key_wrlock(idlen);

	tree_h = &key_tbl[idlen].tree;
	k = key_find(tree_h, id, idlen);
	if (k == NULL) {
		VRT_fail(ctx, "key \"%.*s\" not found", idlen, id);
		KEY_Unlock(idlen);
		return (-1);
	}
	CHECK_OBJ(k, KEY_MAGIC);
	VRBT_REMOVE(key_tree, tree_h, k);
	KEY_Unlock(idlen);

	KEY_Wipe(k->key);

	errno = 0;
	ALLOC_OBJ(addr_ent, ADDR_ENTRY_MAGIC);
	if (addr_ent == NULL) {
		VRT_fail(ctx, "allocating address entry: %s", vstrerror(errno));
		return (-1);
	}
	addr_ent->addr = k->key;
	FREE_OBJ(k);

	Lck_Lock(&page_mtx);
	VSTAILQ_INSERT_TAIL(&addr_head, addr_ent, list);
	Lck_Unlock(&page_mtx);
	return (0);
}

VCL_BOOL
KEY_Exists(uint8_t *id, uint8_t idlen)
{
	struct key_tree *tree_h;
	VCL_BOOL ret;

	AN(id);

	KEY_Rdlock(idlen);
	tree_h = &key_tbl[idlen].tree;
	ret = (key_find(tree_h, id, idlen) != NULL);
	KEY_Unlock(idlen);

	return (ret);
}

enum time {
	ADDED = 0,
	UPDATED,
};

static VCL_TIME
key_time(VRT_CTX, uint8_t *id, uint8_t idlen, enum time t)
{
	struct key_tree *tree_h;
	struct key *k;
	VCL_TIME ret;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(id);

	KEY_Rdlock(idlen);
	tree_h = &key_tbl[idlen].tree;
	if ((k = key_find(tree_h, id, idlen)) == NULL) {
		KEY_Unlock(idlen);
		VRT_fail(ctx, "key \"%.*s\" not found", idlen, id);
		return (0);
	}
	if (t == ADDED)
		ret = k->added;
	else
		ret = k->updated;
	KEY_Unlock(idlen);

	return (ret);
}

VCL_TIME
KEY_Added(VRT_CTX, uint8_t *id, uint8_t idlen)
{
	return (key_time(ctx, id, idlen, ADDED));
}

VCL_TIME
KEY_Updated(VRT_CTX, uint8_t *id, uint8_t idlen)
{
	return (key_time(ctx, id, idlen, UPDATED));
}

#define TIMSZ (sizeof("YYYY-mm-ddTHH:MM:SS+hh:mm"))

void
KEY_Dump(VRT_CTX, enum tz tz)
{
	struct key_tree *tree_h;
	struct key *key;
	struct vsb *vsb = VSB_new_auto();
	time_t tim;
	struct tm tm;
	char tim_buf[TIMSZ];
	const char *p[0];
	struct strands strands = { 1, p };
	struct tm *(*time_r[2])(const time_t *, struct tm *) =
		{localtime_r, gmtime_r};

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	VSB_printf(vsb, "id,added,updated\n");
	for (unsigned i = 0; i < UINT8_MAX; i++) {
		KEY_Rdlock(i);
		tree_h = &key_tbl[i].tree;
		VRBT_FOREACH(key, key_tree, tree_h) {
			CHECK_OBJ_NOTNULL(key, KEY_MAGIC);
			AN(key->id);
			AN(key->added);
			AN(key->updated);

			VSB_bcat(vsb, key->id, key->idlen);

			tim = (time_t)key->added;
			(time_r[tz])(&tim, &tm);
			strftime(tim_buf, TIMSZ, "%FT%T%z", &tm);
			VSB_printf(vsb, ",%s,", tim_buf);

			tim = (time_t)key->updated;
			(time_r[tz])(&tim, &tm);
			strftime(tim_buf, TIMSZ, "%FT%T%z", &tm);
			VSB_printf(vsb, "%s\n", tim_buf);
		}
		KEY_Unlock(i);
	}

	VSB_finish(vsb);
	strands.p[0] = VSB_data(vsb);
	VRT_synth_page(ctx, &strands);
	VSB_destroy(&vsb);
	return;
}
