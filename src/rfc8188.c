/*-
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* Chapter references are to RFC 8188 */

#include <openssl/hmac.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#include <string.h>

#include "rfc8188.h"

#include "vdef.h"
#include "vas.h"

/* ch 2.2 */
static const unsigned char cek_info[] = "Content-Encoding: aes128gcm\0\1";
static const int cek_info_len = sizeof(cek_info) - 1;

/* ch 2.3 */
static const unsigned char nonce_info[] = "Content-Encoding: nonce\0\1";
static const int nonce_info_len = sizeof(nonce_info) - 1;

/*
 * ch 2: "The additional data passed to each invocation of
 * AEAD_AES_128_GCM is a zero-length octet sequence."
 */
static const unsigned char *aad = (const void *)&aad;
static const int aad_len = 0;

static inline void
mk_error(char *buf)
{
	AN(buf);
	ERR_error_string_n(ERR_get_error(), buf, ERRMSG_LEN);
}

void
encode_header(uint8_t *hdr, uint32_t rs, uint8_t idlen, uint8_t *keyid)
{
	vbe32enc(hdr + RS_OFF, rs);
	hdr[IDLEN_OFF] = idlen;
	memcpy(hdr + ID_OFF, keyid, idlen);
}

int
add_salt(unsigned char *hdr, char *errmsg)
{
	if (RAND_bytes(hdr, SALT_LEN) != 1) {
		mk_error(errmsg);
		return (-1);
	}
	return 0;
}

/* ch 2.2 pseudorandom key */
int
derive_prk(uint8_t *salt, uint8_t *key, unsigned char *prk, char *errmsg)
{
	unsigned len;

	AN(salt);
	AN(key);
	AN(prk);
	AN(errmsg);

	if (HMAC(EVP_sha256(), salt, SALT_LEN, key, AES128_KEYLEN, prk, &len)
	    == NULL) {
		mk_error(errmsg);
		return (-1);
	}
	assert(len == SHA256_LEN);

	return 0;
}

/* ch 2.2 content encryption key */
int
derive_cek(unsigned char *prk, unsigned char *cek, char *errmsg)
{
	unsigned len;

	AN(prk);
	AN(cek);
	AN(errmsg);

	if (HMAC(EVP_sha256(), prk, SHA256_LEN, cek_info, cek_info_len, cek,
		&len) == NULL) {
		mk_error(errmsg);
		return (-1);
	}
	assert(len == SHA256_LEN);

	return 0;
}

/* ch 2.3 prenonce to be XOR'd with each record sequence number */
int
derive_prenonce(unsigned char *prk, unsigned char *prenonce, char *errmsg)
{
	unsigned len;

	if (HMAC(EVP_sha256(), prk, SHA256_LEN, nonce_info, nonce_info_len,
		prenonce, &len) == NULL) {
		mk_error(errmsg);
		return (-1);
	}
	assert(len == SHA256_LEN);

	return 0;
}

EVP_CIPHER_CTX *
cipher_ctx_init(int enc, char *errmsg)
{
	EVP_CIPHER_CTX *ctx;

	AN(errmsg);

	if ((ctx = EVP_CIPHER_CTX_new()) == NULL) {
		mk_error(errmsg);
		return (NULL);
	}

	(void)EVP_CIPHER_CTX_set_padding(ctx, 0);

	if (EVP_CipherInit_ex(ctx, EVP_aes_128_gcm(), NULL, NULL, NULL, enc)
	    != 1) {
		mk_error(errmsg);
		return (NULL);
	}
	return ctx;
}

ssize_t
decrypt_record(EVP_CIPHER_CTX *ctx, unsigned char *ciphertext,
    int ciphertext_len, unsigned char *tag, uint8_t *cek, unsigned char *nonce,
    unsigned char *plaintext, int *last, char *errmsg)
{
	int len, plaintext_len;
	unsigned char *end = NULL;

	AN(ctx);
	AN(ciphertext);
	AN(tag);
	AN(cek);
	AN(nonce);
	AN(plaintext);
	AN(last);
	AN(errmsg);
	assert(ciphertext_len >= 0);

	if (EVP_CipherInit_ex(ctx, EVP_aes_128_gcm(), NULL, cek, nonce, -1)
	    != 1) {
		mk_error(errmsg);
		return (-1);
	}

	if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, TAG_LEN, tag)) {
		mk_error(errmsg);
		return (-1);
	}

	if (EVP_CipherUpdate(ctx, NULL, &len, aad, aad_len) != 1) {
		mk_error(errmsg);
		return (-1);
	}

	if (EVP_CipherUpdate(ctx, plaintext, &len, ciphertext,
			      ciphertext_len) != 1) {
		mk_error(errmsg);
		return (-1);
	}
	assert(ciphertext_len == len);
	plaintext_len = len;

	if ((EVP_CipherFinal_ex(ctx, plaintext + plaintext_len, &len)) != 1) {
		mk_error(errmsg);
		return (-1);
	}
	plaintext_len += len;

	*last = -1;
	for (unsigned char *b = plaintext + plaintext_len - 1;
	     *last == -1 && b != plaintext; b--) {
		switch (*b) {
		case '\0':
			continue;
		case '\1':
		case '\2':
			end = b;
			*last = (*b == '\2');
			break;
		default:
			snprintf(errmsg, ERRMSG_LEN, "Ill-formed padding");
			return (-1);
		}
	}
	if (*last == -1) {
		snprintf(errmsg, ERRMSG_LEN, "No delimiter found");
		return (-1);
	}

	assert(end != NULL);
	assert(end >= plaintext);
	return (end - plaintext);
}

ssize_t
encrypt_record(EVP_CIPHER_CTX *ctx, unsigned char *plaintext,
    int plaintext_len, uint32_t rs, uint8_t cek[AES128_KEYLEN],
    unsigned char nonce[NONCE_LEN], int last, unsigned char *ciphertext,
    char errmsg[ERRMSG_LEN])
{
	int len, ciphertext_len;
	uint8_t *tag;

	AN(ctx);
	AN(plaintext);
	AN(cek);
	AN(nonce);
	AN(ciphertext);
	AN(errmsg);
	assert(plaintext_len >= 0);
	assert(rs >= 18);
	assert((unsigned)plaintext_len <= rs - (TAG_LEN + 1));

	if (!last)
		plaintext[plaintext_len] = '\1';
	else
		plaintext[plaintext_len] = '\2';
	for (unsigned i = plaintext_len + 1; i < rs - TAG_LEN; i++)
		plaintext[i] = '\0';

	if (EVP_CipherInit_ex(ctx, EVP_aes_128_gcm(), NULL, cek, nonce, -1)
	    != 1) {
		mk_error(errmsg);
		return (-1);
	}

	if (EVP_CipherUpdate(ctx, NULL, &len, aad, aad_len) != 1) {
		mk_error(errmsg);
		return (-1);
	}

	if (EVP_CipherUpdate(ctx, ciphertext, &len, plaintext, rs - TAG_LEN)
	    != 1) {
		mk_error(errmsg);
		return (-1);
	}
	assert(rs - TAG_LEN == (unsigned)len);
	ciphertext_len = len;

	if ((EVP_CipherFinal_ex(ctx, ciphertext + ciphertext_len, &len)) != 1) {
		mk_error(errmsg);
		return (-1);
	}
	ciphertext_len += len;
	assert((unsigned)ciphertext_len == rs - TAG_LEN);

	tag = ciphertext + ciphertext_len;
	if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, TAG_LEN, tag)) {
		mk_error(errmsg);
		return (-1);
	}

	return (ciphertext_len + TAG_LEN);
}
